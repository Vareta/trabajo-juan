<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(1)) { //si el usuario no tiene permiso al menu Mantenedor usuario (el cual tiene id 1)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else {
        include_once '../php/USRLogConsultas.php';
        añadirLogMenu(1, $_SESSION['usrRut']); //añade un registro de acceso al menu
    }
}

include '../../db/db.php';
include '../../code/utils.php';
?>
<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../js/USRForm&Modal.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">


<head>
    <meta charset="UTF-8">
    <title>Inicio admin</title>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<script>
    $(document).ready(function () {
        crearMenu();
    });
</script>

<table id="usuarios" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Segundo Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Grupo</th>
        <th>Acción</th>
    </tr>
</table>

<script>
    $(document).ready(function () {
        generarDataTableMantenedorUsuario();
    });
</script>
<!-- Button trigger modal añadir usuario -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#añadirUsrModal">
    Añadir
</button>

<!-- Modal añadir usuario-->
<div class="modal fade" id="añadirUsrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir usuario</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="añadirUsrForm" data-toggle="validator" role="form">
                    <div class="form-group has-feedback">
                        <label for="rut" class="col-sm-4 control-label">Rut</label>
                        <div class="col-sm-8">
                            <input type="text" pattern="^([0-9]+-[0-9kK])$" class="form-control" id="rut"
                                   name="rut" placeholder="Ej; 12345678-9"
                                   data-error="El rut contiene caracteres invalidos. Ej de rut valido: 12345678-9"
                                   required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombre" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombre" name="nombre"
                                   placeholder="Alvaro"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="segundoNombre" class="col-sm-4 control-label">Segundo nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="segundoNombre" name="segundoNombre"
                                   placeholder="Aldo"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoPaterno" class="col-sm-4 control-label">Apellido paterno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoPaterno" name="apellidoPaterno"
                                   placeholder="Ramirez"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoMaterno" class="col-sm-4 control-label">Apellido materno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoMaterno" name="apellidoMaterno"
                                   placeholder="Riquelme"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mail" class="col-sm-4 control-label">Correo electronico</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="mail" name="mail"
                                   placeholder="ejemplo@ejemplo.com"
                                   data-error="Correo invalido" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="col-sm-4 control-label">Confirmar Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="confirmPassword"
                                   name="confirmPassword" data-match="#password"
                                   data-match-error="La contraseña no coincide"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadir" class="btn btn-primary" onclick="añadirUsuario('#añadirUsrForm')"
                        name="register">
                    Añadir
                </button>

            </div>
        </div>
    </div>
</div>

<!-- Modal boton ver dentro de la datatable-->
<div class="modal fade bs-example" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     id="gruposAccesoUsuarioModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Grupos acceso</h4>
            </div>
            <div class="modal-body">
                <table id="listaGruposAcceso" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Aplicación</th>
                        <th>Grupo Acceso</th>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Modal editar usuario-->
<div class="modal fade" id="editarUsrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar usuario</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="editarUsrForm" data-toggle="validator" role="form">
                    <div class="form-group has-feedback">
                        <label for="rutEditar" class="col-sm-4 control-label">Rut</label>
                        <div class="col-sm-8">
                            <input type="text" pattern="^([0-9]+-[0-9kK])$" class="form-control" id="rutEditar"
                                   name="rutEditar" readonly required>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombreEditar" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombreEditar" name="nombreEditar"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="segundoNombreEditar" class="col-sm-4 control-label">Segundo nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="segundoNombreEditar" name="segundoNombreEditar"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoPaternoEditar" class="col-sm-4 control-label">Apellido paterno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoPaternoEditar"
                                   name="apellidoPaternoEditar"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoMaternoEditar" class="col-sm-4 control-label">Apellido materno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoMaternoEditar"
                                   name="apellidoMaternoEditar"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mailEditar" class="col-sm-4 control-label">Correo electronico</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="mailEditar" name="mailEditar"
                                   data-error="Correo invalido" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="passwordEditar" class="col-sm-4 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="passwordEditar" name="passwordEditar"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPasswordEditar" class="col-sm-4 control-label">Confirmar Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="confirmPasswordEditar"
                                   name="confirmPassword" data-match="#passwordEditar"
                                   data-match-error="La contraseña no coincide"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadirEditar" class="btn btn-primary"
                        onclick="editarUsuario('#editarUsrForm')" name="registerEditar">
                    Editar
                </button>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    iniciarUsrModal('#añadirUsrModal');
</script>

</body>
</html>
