<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(2)) { //si el usuario no tiene permiso al menu "Menu" (el cual tiene id 2)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else {
        include_once '../php/USRLogConsultas.php';
        añadirLogMenu(2, $_SESSION['usrRut']); //añade un registro de acceso al menu
    }
}
?>

<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<head>
    <meta charset="UTF-8">
    <title>Menu</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<form class="form" method="post" id="aplicacionesForm" role="form">
    <div class="form-group">
        <label for="aplicaciones" class="control-label">Aplicaciones</label>
        <select class="form-control" id="aplicaciones" name="aplicaciones">
        </select>
    </div>
</form>
<div id="contenedorDataTable">
<table id="listaMenu" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>ID</th>
        <th>Titulo</th>
        <th>URL</th>
        <th>Sub menu</th>
        <th>Accion</th>
    </tr>
</table>
</div>

<div class="alert alert-info" role="alert" id="errorDataTable" hidden></div>
<script>
    crearMenu();
    crearSelectAplicaciones();
    onAplicacionesSelectChange();
    generarDataTableMenu();
</script>

<!-- Modal boton ver dentro de la datatable-->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     id="subMenusModal">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sub Menu</h4>
            </div>
            <div class="modal-body">
                <ul class="list-group" id="subMenuLista">
                </ul>
            </div>
        </div>
    </div>
</div>
</body>

<div class="modal fade" id="añadirSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir Submenu</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="añadirSubMenuForm" data-toggle="validator" role="form">
                    <div class="form-group">
                        <label for="idPadreSubMenu" class="col-sm-4 control-label" hidden>idPadre</label>
                        <input type="hidden" class="form-control" id="idPadreSubMenu" name="idPadreSubMenu"
                               data-error="Este campo es obligatorio">
                    </div>
                    <div class="form-group">
                        <label for="idAppSubMenu" class="col-sm-4 control-label" hidden>idPadre</label>
                        <input type="hidden" class="form-control" id="idAppSubMenu" name="idAppSubMenu"
                               data-error="Este campo es obligatorio">
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombreSubMenu" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombreSubMenu" name="nombreSubMenu"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="urlSubMenu" class="col-sm-4 control-label">Url</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="urlSubMenu" name="urlSubMenu"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadirSubMenu" class="btn btn-primary"
                        onclick="añadirSubMenu('#añadirSubMenuForm')" name="registerAñadir">
                    Añadir
                </button>

            </div>
        </div>
    </div>
</div>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#añadirMenuModal">
    Añadir
</button>

<div class="modal fade" id="añadirMenuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir menu</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="añadirMenuForm" data-toggle="validator" role="form">

                    <div class="form-group">
                        <label for="idAppMenu" class="col-sm-4 control-label" hidden>idAppMenu</label>
                        <input type="hidden" class="form-control" id="idAppMenu" name="idAppMenu"
                               data-error="Este campo es obligatorio">
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombreMenu" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombreMenu" name="nombreMenu"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="urlMenu" class="col-sm-4 control-label">Url</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="urlMenu" name="urlMenu"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadirSubMenu" class="btn btn-primary"
                        onclick="añadirMenu('#añadirMenuForm')" name="registerAñadir">
                    Añadir
                </button>

            </div>
        </div>
    </div>
</div>

</html>
