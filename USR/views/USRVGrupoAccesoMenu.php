<?php
if (!isset($_SESSION)) {
    session_start();
}

if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(3)) { //si el usuario no tiene permiso al menu Grupo Acceso (el cual tiene id 3)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else if (isset($_GET['GAId']) && isset($_GET['appId'])) {
        if (usrTieneAccesoAGrupoAcceso($_GET['GAId'], $_GET['appId'])) {
            $_SESSION['appIdAux'] = $_GET['appId'];
            $_SESSION['grupoAccesoId'] = $_GET['GAId'];
        } else {
            header("Location: USRVError.php");
        }
    } else {
        header("Location: USRVError.php");
    }
}

?>


<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<head>
    <meta charset="UTF-8">
    <title>Grupo Acceso</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<div id="contenedorDataTable">
<table id="listaMenuPadre" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Seleccion</th>
        <th>Titulo</th>
        <th>Sub menu</th>
    </tr>
</table>
</div>

<div class="alert alert-info" role="alert" id="errorDataTable" hidden></div>
<script>
    crearMenu();
    generarDataTableGrupoAccesoMenu();

</script>

<button type="button" class="btn btn-primary btn-lg" onclick="guardarCambios()">
    Guardar cambios
</button>

<div class="modal fade" id="añadirSubMenuModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir submenu</h4>
            </div>
            <div class="modal-body">
                <table id="listaSubMenuDePadre" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Seleccion</th>
                        <th>Nombre</th>
                    </tr>
                </table>
                <div id="msjeError">

                </div>
                <script>
                    GrupoAccesoMenuListeners();
                </script>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadir" class="btn btn-primary" onclick="añadirSubMenuGAMenubtnVer()"
                        name="register">
                    Guardar cambios
                </button>

            </div>
        </div>
    </div>
</div>

</html>