<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(4)) { //si el usuario no tiene permiso al menu Mantenedor acceso (el cual tiene id 4)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else if (isset($_GET['GAId']) && isset($_GET['appId'])) {
        if (usrTieneAccesoAGrupoAcceso($_GET['GAId'], $_GET['appId'])) {
            $_SESSION['appIdAux'] = $_GET['appId'];
            $_SESSION['grupoAccesoId'] = $_GET['GAId'];
        } else {
            header("Location: USRVError.php");
        }
    } else {
        header("Location: USRVError.php");
    }
}
include '../php/GPAConsultas.php';
?>

<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<head>
    <meta charset="UTF-8">
    <title>Mantenedor de acceso detalle</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<div id="contenedorDataTable">
<table id="listaUsuarios" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Eliminar</th>
    </tr>
</table>
</div>
<div class="alert alert-info" role="alert" id="errorDataTable" hidden></div>
<script>
    crearMenu();
    generarDataTableMantenedorAccesoDetalle();
</script>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#añadirGrupoAccesoModal">
    Añadir usuario
</button>

<div class="modal fade" id="añadirGrupoAccesoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir menu</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="buscarUsuario" data-toggle="validator" role="form">
                    <div class="form-group has-feedback">
                        <label for="rut" class="col-sm-4 control-label">Buscar usuario por rut</label>
                        <div class="col-sm-8">
                            <input type="text" pattern="^([0-9]+-[0-9kK])$" class="form-control" id="rut"
                                   name="rut" placeholder="Ej; 12345678-9"
                                   data-error="El rut contiene caracteres invalidos. Ej de rut valido: 12345678-9"
                                   required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>

                <div class="panel panel-default" id="panelUsuario" hidden>
                    <div class="panel-heading">
                        <h3 class="panel-title">Resultado busqueda</h3>
                    </div>
                    <div class="panel-body">
                        <div id="usuarioEncontrado" hidden>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Rut</span>
                                    <input type="text" class="form-control" id="rutResultado" aria-describedby="basic-addon1" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Nombre</span>
                                    <input type="text" class="form-control" id="nombreResultado" aria-describedby="basic-addon1" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">Empresa</span>
                                    <input type="text" class="form-control" id="empresaResultado" aria-describedby="basic-addon1" readonly>
                                </div>
                            </div>
                            <button type="button" id="btnAñadirResultado" class="btn btn-primary pull-right"
                                    onclick="btnAñadirResultadoBusqueda()" name="btnAñadir">
                                Añadir
                            </button>
                        </div>
                        <div id="errorUsuario" hidden>El usuario no existe
                        </div>
                    </div>

                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadirSubMenu" class="btn btn-primary"
                        onclick="buscarUsuarioMADetalle('#buscarUsuario')" name="registerAñadir">
                    Buscar
                </button>

            </div>
        </div>
    </div>
</div>

</html>