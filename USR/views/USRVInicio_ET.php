<?php
if(!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
}
include_once '../php/USRConsultas.php';
$_SESSION['appId'] = 1;

if (!usuarioTieneMenu($_SESSION['usrRut'], $_SESSION['appId'])) { //si el usuario no cuenta con acceso a menu
    header("Location: USRVError.php?error=noMenu");
}
else {
    include_once '../php/USRLogConsultas.php';
    añadirLogAplicacion($_SESSION['appId'], $_SESSION['usrRut']); //añade la aplicacion al log
    header("Location: USRVInicio.php");
}
?>