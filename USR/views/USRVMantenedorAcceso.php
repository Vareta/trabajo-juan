<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(4)) { //si el usuario no tiene permiso al menu Mantenedor acceso (el cual tiene id 4)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else {
        include_once '../php/USRLogConsultas.php';
        añadirLogMenu(4, $_SESSION['usrRut']); //añade un registro de acceso al menu
    }
}
?>

<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<head>
    <meta charset="UTF-8">
    <title>Mantenedor de Acceso</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<form class="form" method="post" id="aplicacionesForm" role="form">
    <div class="form-group">
        <label for="aplicaciones" class="control-label">Aplicaciones</label>
        <select class="form-control" id="aplicaciones" name="aplicaciones">
        </select>
    </div>
</form>
<script>


</script>
<div id="contenedorDataTable">
<table id="listaGrupoAcceso" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Nombre Grupo</th>
        <th>Ver / Agregar</th>
    </tr>
</table>
</div>

<div class="alert alert-info" role="alert" id="errorDataTable" hidden></div>
<script>
    crearMenu();
    crearSelectAplicaciones();
    generarDataTableMantenedorAcceso();
    onAplicacionesSelectGrupoAccesoChange();
</script>


</html>
