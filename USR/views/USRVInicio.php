<?php
include_once '../php/USRConsultas.php';
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
}
if (!usuarioTieneMenu($_SESSION['usrRut'], $_SESSION['appId'])) { //si el usuario no cuenta con acceso a algun menu
    header("Location: USRVError.php?error=noMenu");
}
?>


<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" href="../../css/bootstrap.min.css">

<head>
    <meta charset="UTF-8">
    <title>Inicio admin</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>
<script>
    $(document).ready(function () {
        crearMenu();
    });
</script>
</body>
</html>