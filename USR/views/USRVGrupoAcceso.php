<?php
if (!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut']) || !isset($_SESSION['appId'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
} else {
    include_once '../php/USRPermisosMenu.php';
    if (!usrTieneAccesoAMenuActual(3)) { //si el usuario no tiene permiso al menu Grupo Acceso (el cual tiene id 3)
        header('Location: ../../USR/views/USRVError.php?noPermiso');
    } else {
        include_once '../php/USRLogConsultas.php';
        añadirLogMenu(3, $_SESSION['usrRut']); //añade un registro de acceso al menu
    }
}
?>


<!DOCTYPE html>
<html>
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/USRMenu.js"></script>
<script type="text/javascript" src="../js/USRDatatable.js"></script>
<script type="text/javascript" src="../../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../../js/dataTableSpanish.js"></script>
<script type="text/javascript" src="../../js/boostrapValidator.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min"></script>

<link rel="stylesheet" type="text/css" href="../../css/jquery.dataTables.min.css"/>
<link rel="stylesheet" href="../../css/bootstrap.min.css">
<head>
    <meta charset="UTF-8">
    <title>Grupo Acceso</title>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="USRVInicio.php">Inicio</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
        </ul>
        <div class="nav navbar-nav navbar-right">
            <a href="../php/USRLogout.php" class="navbar-brand"><strong>Logout</strong></a>
        </div>
    </div>
</nav>

<form class="form" method="post" id="aplicacionesForm" role="form">
    <div class="form-group">
        <label for="aplicaciones" class="control-label">Aplicaciones</label>
        <select class="form-control" id="aplicaciones" name="aplicaciones">
        </select>
    </div>
</form>
<script>


</script>
<div id="contenedorDataTable">
<table id="listaGrupoAcceso" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Ver/Modificar</th>
        <th>Eliminar</th>
    </tr>
</table>
</div>

<div class="alert alert-info" role="alert" id="errorDataTable" hidden></div>
<script>
    crearMenu();
    crearSelectAplicaciones();
    onAplicacionesSelectGrupoAccesoChange();
    generarDataTableGrupoAcceso();
</script>

<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#añadirGrupoAccesoModal">
    Añadir
</button>

<div class="modal fade" id="añadirGrupoAccesoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir menu</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="añadirGrupoAccesoForm" data-toggle="validator" role="form">

                    <div class="form-group">
                        <label for="idApp" class="col-sm-4 control-label" hidden>idAppMenu</label>
                        <input type="hidden" class="form-control" id="idApp" name="idApp"
                               data-error="Este campo es obligatorio">
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombreMenu" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombreMenu" name="nombreMenu"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadirSubMenu" class="btn btn-primary"
                        onclick="añadirGrupoAcceso('#añadirGrupoAccesoForm')" name="registerAñadir">
    Añadir
                </button>

            </div>
        </div>
    </div>
</div>

</html>