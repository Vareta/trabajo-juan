DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_menu`(IN `MENUID` INT)
MODIFIES SQL DATA
	BEGIN
		DELETE FROM usr_menu WHERE N_MNUID = MENUID;
		DELETE FROM usr_menu WHERE USR_N_MNUID = MENUID;
		DELETE FROM usr_detallegrupoacceso WHERE N_MNUID = MENUID;
	END$$
DELIMITER ;