function iniciarUsrModal(modalId) {
    $(modalId).on('shown.bs.modal', function () { //para cuando inicial el modal.
        $('#rut').focus();
    });
    $(modalId).validator();//setea las validaciones

    $(modalId).on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    });
}

function añadirUsuario(formId) { //funcion para el botton Añadir en la modal de añadir usuario
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        var formData = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "../php/USRConsultas.php",
            data: {actionUSR: 'añadirUsuario', "formData": formData},
            success: function (response) {
                if (response.localeCompare("existeUsuario") == 0) {
                    bootbox.alert("Ya existe un usuario bajo ese rut.")
                } else if (response.localeCompare("existeMail") == 0) {
                    bootbox.alert("Ya existe un usuario con ese correo electronico.")
                } else if (response.localeCompare("rutUsrInvalido") == 0) {
                    bootbox.alert("El rut del usuario es invalido.")
                } else {
                    bootbox.alert("Usuario añadido correctamente.", function () {
                        $('#añadirUsrModal').modal('toggle');
                        $('#usuarios').DataTable().ajax.reload();
                    });

                }
            }
        });
    }
}


/**********************************************************************************************************************
 ********************************************************* Login forrm ************************************************
 **********************************************************************************************************************/

/**
 * Recopila los datos del form para el ingreso de sesion y los en via para su verificacion
 * @param formId: Form con los datos a enviar
 */
function ingresar(formId) {
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        var formData = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "code/login/login.php",
            data: {formData: formData},
            success: function (response) {
                if (response.localeCompare("noValida") == 0) {
                    document.getElementById("errorLogin").textContent="Rut del usuario incorrecto. Vuelva a intentar";
                    $('#errorLogin').show();
                } else if (response.localeCompare("noExiste") == 0) {
                    document.getElementById("errorLogin").textContent="El usuario no existe";
                    $('#errorLogin').show();
                } else if (response.localeCompare("pwdIncorrecta") == 0) {
                    document.getElementById("errorLogin").textContent="Contraseña incorrecta";
                    $('#errorLogin').show();
                } else if (response.localeCompare("exito") == 0) {
                    window.location.href = "../../PER/views/PERVInicio.php";
                }
            }
        });
    }
}