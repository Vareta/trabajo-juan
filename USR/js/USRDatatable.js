/**********************************************************************************************************************
 ************************************* DATATABLE PARA USRVMantenedorUsuario.php ***************************************
 **********************************************************************************************************************/


function generarDataTableMantenedorUsuario() {
    $(document).ready(function () {
        var dataTable = $('#usuarios').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/USRConsultas.php",
                "data": {actionUSR: "getUsuariosAplicacion"}
            },
            "columns": [
                {"data": "N_USRRUT"},
                {"data": "S_USRPRIMERNOMBRE"},
                {"data": "S_USRSEGUNDONOMBRE"},
                {"data": "S_USRAPELLIDOPATERNO"},
                {"data": "S_USRAPELLIDOMATERNO"},
                {"defaultContent": "<button type='button' class='ver btn btn-info'>ver</button>"},
                {
                    "defaultContent": "<button type='button' class='editar btn btn-primary'>editar</button> " +
                    "<button type='button' class='eliminar btn btn-danger''>eliminar</button>"
                }
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return data + ' - ' + row['S_USRDIGITOVERIFICADOR'];
                    },
                    "targets": 0
                }
            ]
        });
        obtenerRutFila('#usuarios', dataTable);
        obenerDatosAEditar('#usuarios', dataTable);
        btnEliminar('#usuarios', dataTable);
        resetearModals();
    });
}


function obtenerRutFila(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.ver', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#gruposAccesoUsuarioModal').modal('show'); //inicia la modal
        crearModalBtnVerMantenedorUsuario(data.N_USRRUT);
    });
}

function btnEliminar(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.eliminar', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        bootbox.confirm({
            title: "Eliminar usuario del grupo actual",
            message: "Esta seguro de eliminar a "+ data.S_USRPRIMERNOMBRE + ' ' + data.S_USRSEGUNDONOMBRE + ' ' +
            data.S_USRAPELLIDOPATERNO + ' ' + data.S_USRAPELLIDOMATERNO +"?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "POST",
                        url: "../php/USRConsultas.php",
                        data: {actionUSR: 'eliminarUsuario', "rut": data.N_USRRUT},
                        success: function (response) {
                            console.log(response);
                            $('#usuarios').DataTable().ajax.reload();
                        }
                    });
                }

            }
        });
    });
}

function crearModalBtnVerMantenedorUsuario(usrRut) {

    $('#listaGruposAcceso').DataTable({
        "ajax": {
            "method": "GET",
            "url": "../php/USRConsultas.php",
            "data": {actionUSR: 'getGruposAccesoUsuario', rut: usrRut}
        },
        "columns": [
            {"data": "S_APPNOMBRE"},
            {"data": "S_GPANOMBRE"}
        ],
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
        },
        "bDestroy": true,
        "paging":   false,
        "info":     false,
        "searching": false
    });
}


function resetearModals() {
    $('#gruposAccesoUsuarioModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $('#gruposAccesoUsuarioLista').empty();
    });
    $('#editarUsrModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    });
}

function obenerDatosAEditar(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.editar', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#rutEditar').val(data.N_USRRUT + '-' + data.S_USRDIGITOVERIFICADOR);
        $('#nombreEditar').val(data.S_USRPRIMERNOMBRE);
        $('#segundoNombreEditar').val(data.S_USRSEGUNDONOMBRE);
        $('#apellidoPaternoEditar').val(data.S_USRAPELLIDOPATERNO);
        $('#apellidoMaternoEditar').val(data.S_USRAPELLIDOMATERNO);
        $('#mailEditar').val(data.S_USREMAIL);
        $('#editarUsrModal').modal('show'); //inicia la modal
    });
}

function editarUsuario(formId) { //funcion para el botton Añadir en la modal de añadir usuario
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        var formData = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "../php/USRConsultas.php",
            data: {actionUSR: 'editarUsuario', "formData": formData},
            success: function (response) {
                $('#editarUsrModal').modal('toggle');
                $('#usuarios').DataTable().ajax.reload();
            }
        });
    }
}

/**********************************************************************************************************************
 ******************************************** DATATABLE PARA USRVMenu.php *********************************************
 **********************************************************************************************************************/

function generarDataTableMenu() {
    $(document).ready(function () {
        var dataTable = $('#listaMenu').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/MNUConsultas.php",
                "data": {actionMNU: 'getMenuPadreByApp'}
            },
            "columns": [
                {"data": "N_MNUID"},
                {"data": "S_MNUNOMBRE"},
                {"data": "S_MNUURL"},
                {
                    "defaultContent": "<button type='button' class='ver btn btn-info'>ver</button> " +
                    "<button type='button' class='añadir btn btn-primary''>crear</button>"
                },
                {"defaultContent": "<button type='button' class='eliminar btn btn-danger'>eliminar</button>"}
            ],
            "createdRow": function (row, data, index) {
                if (data['N_MNUID'] == -1) { //cuando no hay usuarios
                    $('#contenedorDataTable').hide();
                    document.getElementById("errorDataTable").textContent="La aplicación no tiene menus disponibles";
                    $('#errorDataTable').show();
                } else {
                    $('#contenedorDataTable').show();
                    $('#errorDataTable').hide();
                }

            },
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true
        });
        obtenerIdMenuFila('#listaMenu', dataTable);
        obtenerPadreMenuId('#listaMenu', dataTable);
        btnEliminarMenu('#listaMenu', dataTable);
        resetearMenuModals();
    });
}

function obtenerIdMenuFila(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.ver', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#subMenusModal').modal('show'); //inicia la modal
        crearModalBotonMenuVer(data.N_MNUID);
    });
}

function crearModalBotonMenuVer(menuId) {
    $.ajax({
        type: "GET",
        url: "../php/MNUConsultas.php",
        data: {actionMNU: 'getSubMenusDeMenuPadre', menuId: menuId},
        success: function (response) {
            var jsonResponse = JSON.parse(response);
            if (typeof jsonResponse[0].S_MNUNOSUBMENU !== 'undefined' || jsonResponse[0].S_MNUNOSUBMENU) { //si no tiene submenus asociados
                añadirSubMenuListaVer(jsonResponse[0].S_MNUNOSUBMENU);
            } else {
                for (var i = 0; i < jsonResponse.length; i++) {
                    añadirSubMenuListaVer(jsonResponse[i].S_MNUNOMBRE);
                }
            }
        }
    });
}

function btnEliminarMenu(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.eliminar', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        bootbox.confirm({
            title: "Eliminar usuario del grupo actual",
            message: "Esta seguro de eliminar "+ data.S_MNUNOMBRE +"?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "POST",
                        url: "../php/MNUConsultas.php",
                        data: {actionMNU: 'eliminarMenu', "menuId": data.N_MNUID},
                        success: function (response) {
                            generarDataTableMenuByAppId(); //basicamente actua como reload de la app actual
                        }
                    });
                }

            }
        });
    });
}

function añadirSubMenuListaVer(nombreSubMenu) {
    var padre = document.getElementById('subMenuLista');//obtiene el elemento al cual se va a anclar
    var newElement = document.createElement('li');
    newElement.setAttribute('class', 'list-group-item');
    var text = document.createTextNode(nombreSubMenu);
    newElement.append(text);
    padre.appendChild(newElement);
}

function resetearMenuModals() {
    $('#subMenusModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $('#subMenuLista').empty();
    });
    $('#añadirSubMenuModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    });
    $('#añadirMenuModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    });
}

function obtenerPadreMenuId(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.añadir', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#idPadreSubMenu').val(data.N_MNUID);
        $('#idAppSubMenu').val(data.N_APPID);
        $('#añadirSubMenuModal').modal('show'); //inicia la modal
    });
}

function añadirSubMenu(formId) {
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        var data = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "../php/MNUConsultas.php",
            data: {actionMNU: 'añadirSubmenu', formData: data},
            success: function (response) {
                if (response.localeCompare('exito') == 0) {
                    $('#añadirSubMenuModal').modal('toggle');
                }
            }
        });
    }
}

function añadirMenu(formId) {
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        $('#idAppMenu').val(getAppId());
        var data = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "../php/MNUConsultas.php",
            data: {actionMNU: 'añadirMenu', formData: data},
            success: function (response) {
                if (response.localeCompare('exito') == 0) {
                    $('#añadirMenuModal').modal('toggle');
                    generarDataTableMenuByAppId(); //basicamente actua como reload de la app actual
                }
            }
        });
    }
}

function getAppId() {
    var appSelect = document.getElementById("aplicaciones");
    return appSelect.options[appSelect.selectedIndex].value;
}

function crearSelectAplicaciones() {
    $.ajax({
        type: "GET",
        url: "../php/USRConsultas.php",
        data: {actionUSR: 'getAplicacionesUsuario'},
        success: function (response) {
            var jsonResponse = JSON.parse(response);
            for (i = 0; i < jsonResponse.length; i++) {
                if (i == 0) {
                    añadirAplicacionesSelect(jsonResponse[i].N_APPID, jsonResponse[i].S_APPNOMBRE, true)
                } else {
                    añadirAplicacionesSelect(jsonResponse[i].N_APPID, jsonResponse[i].S_APPNOMBRE, false)
                }
            }
        }
    });
}

function añadirAplicacionesSelect(appId, nombre, primero) {
    var padre = document.getElementById('aplicaciones');//obtiene el elemento al cual se va a anclar
    var newElement = document.createElement('option');
    if (primero) {
        newElement.setAttribute('selected', 'selected');
    }
    newElement.setAttribute('value', appId);
    var text = document.createTextNode(nombre);
    newElement.append(text);
    padre.appendChild(newElement);
}

function onAplicacionesSelectChange() {
    $('#aplicaciones').change(function () {
        generarDataTableMenuByAppId()
    });
}

function generarDataTableMenuByAppId() {
    var appId = getAppId();
    $.ajax({
        type: "GET",
        url: "../php/MNUConsultas.php",
        data: {actionMNU: 'getMenuPadreByAppOtorgada', appId: appId},
        success: function (response) {
            var jsonResponse = JSON.parse(response);
            $('#listaMenu').DataTable().clear().rows.add(jsonResponse.data).draw()
        }
    });
}

/**********************************************************************************************************************
 *************************************** DATATABLE PARA USRVGrupoAcceso.php *******************************************
 **********************************************************************************************************************/

function generarDataTableGrupoAcceso() {
    $(document).ready(function () {
        var dataTable = $('#listaGrupoAcceso').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/GPAConsultas.php",
                "data": {actionGPA: 'getGrupoAccesoDefault'}
            },
            "columns": [
                {"data": "N_GPASECUENCIAL"},
                {"data": "S_GPANOMBRE"},
                {"defaultContent": "<button type='button' class='ver btn btn-info'>ver</button> "},
                {"defaultContent": "<button type='button' class='eliminar btn btn-danger'>eliminar</button>"}
            ],
            "createdRow": function (row, data, index) {
                if (data['N_GPASECUENCIAL'] == -1) { //cuando no hay usuarios
                    $('#contenedorDataTable').hide();
                    document.getElementById("errorDataTable").textContent="La aplicación no tiene grupos de acceso disponibles";
                    $('#errorDataTable').show();
                } else {
                    $('#contenedorDataTable').show();
                    $('#errorDataTable').hide();
                }

            },
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true
        });
        resetearGrupoAccesoModals();
        obtenerIdGrupoAccesoFila('#listaGrupoAcceso', dataTable);
        btnEliminarGrupoAcceso('#listaGrupoAcceso', dataTable);
    });
}

function onAplicacionesSelectGrupoAccesoChange() {
    $('#aplicaciones').change(function () {
        actualizarDataTableGrupoAccesoByAppId()
    });
}

function actualizarDataTableGrupoAccesoByAppId() {
    var appId = getAppId();
    table = $('#listaGrupoAcceso').DataTable();
    table.clear();
    $.ajax({
        type: "GET",
        url: "../php/GPAConsultas.php",
        data: {actionGPA: 'getGrupoAccesoByAppId', appId: appId},
        success: function (response) {
            table.rows.add(JSON.parse(response).data).draw();
        }
    });
}


function añadirGrupoAcceso(formId) {
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        $('#idApp').val(getAppId());
        var data = JSON.stringify($(formId).serializeArray());
        $.ajax({
            type: "POST",
            url: "../php/GPAConsultas.php",
            data: {actionGPA: 'añadirGrupoAcceso', formData: data},
            success: function (response) {
                if (response.localeCompare('exito') == 0) {
                    $('#añadirGrupoAccesoModal').modal('toggle');
                    actualizarDataTableGrupoAccesoByAppId(); //basicamente actua como reload de la datatable con la app actual
                }
            }
        });
    }
}

function btnEliminarGrupoAcceso(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.eliminar', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        bootbox.confirm({
            title: "Eliminar usuario del grupo actual",
            message: "Esta seguro de eliminar "+ data.S_GPANOMBRE +"?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "POST",
                        url: "../php/GPAConsultas.php",
                        data: {actionGPA: 'eliminarGrupoAcceso', "grupoAccesoId": data.N_GPASECUENCIAL},
                        success: function (response) {
                            actualizarDataTableGrupoAccesoByAppId(); //basicamente actua como reload de la datatable con la app actual
                        }
                    });
                }

            }
        });
    });
}

function resetearGrupoAccesoModals() {
    $('#añadirGrupoAccesoModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    });
}

function obtenerIdGrupoAccesoFila(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.ver', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        window.location.href = "USRVGrupoAccesoMenu.php?GAId=" + encodeURIComponent(data.N_GPASECUENCIAL)
                                + "&appId=" + encodeURIComponent(data.N_APPID);
    });
}

/**********************************************************************************************************************
 ************************************* DATATABLE PARA USRVGrupoAccesoMenu.php ******************************************
 **********************************************************************************************************************/

function generarDataTableGrupoAccesoMenu() {
    $(document).ready(function () {
        var dataTable = $('#listaMenuPadre').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/MNUConsultas.php",
                "data": {actionMNU: 'getMenuByappId'}
            },
            "columns": [
                {"defaultContent": ""},
                {"data": "S_MNUNOMBRE"},
                {"defaultContent": "<button type='button' class='ver btn btn-info'>ver</button> "}
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true,
            "columnDefs": [ {
                'searchable':false,
                'orderable':false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
                "targets":   0
            } ],
            "createdRow": function ( row, data, index ) {

                if (data['N_MNUID'] == -1) { //cuando no hay menu
                    $('#contenedorDataTable').hide();
                    document.getElementById("errorDataTable").textContent="El grupo de acceso no tiene menus disponibles";
                    $('#errorDataTable').show();
                } else {
                    if (data['N_ESTAASIGNADO']) {
                        $('input[type="checkbox"]', row).prop('checked', true);
                    }
                    $('#contenedorDataTable').show();
                    $('#errorDataTable').hide();
                }

            }
        });


        $('#listaMenuPadre tbody').on('change', 'input[type="checkbox"]', function(){
            var data = dataTable.row($(this).parents("tr")).data();
            if(this.checked){
                data.N_ESTAASIGNADO = true;
            } else {
                data.N_ESTAASIGNADO = false;
            }
        });

        btnVerGrupoAccesoMenu('#listaMenuPadre', dataTable);
    });
}

function guardarCambios() {
    bootbox.confirm({
        title: "Añadir Menu",
        message: "Esta seguro que desea realizar estos cambios?",
        buttons: {
            confirm: {
                label: 'Confirmar',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === true) {
                tableData = JSON.stringify($('#listaMenuPadre').DataTable().data());
                $.ajax({
                    type: "POST",
                    url: "../php/GPAConsultas.php",
                    data: {actionGPA: 'modificarGrupoAccesoDetalle', "tableData": tableData},
                    success: function (response) {
                        console.log(response);
                    }
                });
            }

        }
    });

}

function btnVerGrupoAccesoMenu(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.ver', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#añadirSubMenuModal').modal('show'); //inicia la modal
        crearModalBtnVerGrupoAccesoMenu(data.N_MNUID);
    });
}

function crearModalBtnVerGrupoAccesoMenu(menuId) {

        var dataTableBtnVerGAMenu = $('#listaSubMenuDePadre').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/MNUConsultas.php",
                "data": {actionMNU: 'getSubMenusDataTableGrupoAccesoMenu', menuId: menuId}
            },
            "columns": [
                {"defaultContent": ""},
                {"data": "S_MNUNOMBRE"}
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true,
            "columnDefs": [{
                'searchable': false,
                'orderable': false,
                'className': 'dt-body-center',
                'render': function (data, type, full, meta) {
                    return '<input type="checkbox">';
                },
                "targets": 0
            }],
            "createdRow": function (row, data, index) {
                if (data['N_ESTAASIGNADO']) {
                    $('input[type="checkbox"]', row).prop('checked', true);
                }

            },
            "paging":   false,
            "info":     false,
            "searching": false
        });
}

function creaListaErrorBtnVerGAMenu(mensaje) {
    var padre = document.getElementById('msjeError');//obtiene el elemento al cual se va a anclar
    var ulElement = document.createElement('ul');
    ulElement.setAttribute('class', 'list-group');
    var liElement = document.createElement('li');
    liElement.setAttribute('class', 'list-group-item');
    var text = document.createTextNode(mensaje);
    liElement.append(text);
    ulElement.appendChild(liElement);
    padre.appendChild(ulElement);
}

function GrupoAccesoMenuListeners() {
    $('#listaSubMenuDePadre').on( 'init.dt', function () {
        $('#listaSubMenuDePadre').show();
        var datos = $('#listaSubMenuDePadre').DataTable().data()[0];
        if (datos.N_MNUID == -1) {
            $('#listaSubMenuDePadre').hide();
            creaListaErrorBtnVerGAMenu(datos.S_MNUNOMBRE);
        }

        $('#listaSubMenuDePadre tbody').on('change', 'input[type="checkbox"]', function(){
            var data = $('#listaSubMenuDePadre').DataTable().row($(this).parents("tr")).data();
            if(this.checked){
                console.log('checked');
                data.N_ESTAASIGNADO = true;
            } else {
                data.N_ESTAASIGNADO = false;
                console.log('unchecked');
            }
        });
    } );

    $('#añadirSubMenuModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        console.log('cerrar');
        $('#msjeError').children().remove();
    });
}

function añadirSubMenuGAMenubtnVer() {
    bootbox.confirm({
        title: "Añadir Menu",
        message: "Esta seguro que desea realizar estos cambios?",
        buttons: {
            confirm: {
                label: 'Confirmar',
                className: 'btn-success'
            },
            cancel: {
                label: 'Cancelar',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result === true) {
                tableData = JSON.stringify($('#listaSubMenuDePadre').DataTable().data());
                $.ajax({
                    type: "POST",
                    url: "../php/GPAConsultas.php",
                    data: {actionGPA: 'asociarSubMenuAGrupoAcceso', "tableData": tableData},
                    success: function (response) {
                        console.log(response);
                        $('#añadirSubMenuModal').modal('toggle');
                        $('#listaMenuPadre').DataTable().ajax.reload();
                    }
                });
            }

        }
    });
}

/**********************************************************************************************************************
 ************************************** DATATABLE PARA USRVMantenedorAcceso.php ****************************************
 **********************************************************************************************************************/

function generarDataTableMantenedorAcceso() {
    $(document).ready(function () {
        var dataTable = $('#listaGrupoAcceso').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/GPAConsultas.php",
                "data": {actionGPA: 'getGrupoAccesoDefault'}
            },
            "columns": [
                {"data": "S_GPANOMBRE"},
                {"defaultContent": "<button type='button' class='ver btn btn-info'>Link</button> "}
            ],
            "createdRow": function (row, data, index) {
            if (data['N_GPASECUENCIAL'] == -1) { //cuando no hay grupo de acceso
                $('#contenedorDataTable').hide();
                document.getElementById("errorDataTable").textContent="La aplicación no tiene grupos de acceso disponibles";
                $('#errorDataTable').show();
            } else {
                $('#contenedorDataTable').show();
                $('#errorDataTable').hide();
            }

        },
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true
        });
        btmVerMantenedorAcceso('#listaGrupoAcceso', dataTable)

    });
}

function btmVerMantenedorAcceso(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.ver', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        $('#añadirSubMenuModal').modal('show'); //inicia la modal
        window.location.href = "USRVMantenedorAccesoDetalle.php?GAId=" + encodeURIComponent(data.N_GPASECUENCIAL)
            + "&appId=" + encodeURIComponent(data.N_APPID);
    });
}

/**********************************************************************************************************************
 ********************************* DATATABLE PARA USRVMantenedorAccesoDetalle.php *************************************
 **********************************************************************************************************************/

function generarDataTableMantenedorAccesoDetalle() {
    $(document).ready(function () {
        var dataTable = $('#listaUsuarios').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../php/GPAConsultas.php",
                "data": {actionGPA: 'getUsuariosGrupoAcceso'}
            },
            "columns": [
                {"data": "N_USRRUT"},
                {"data": "S_GPANOMBRE"},
                {"defaultContent": "<button type='button' class='eliminar btn btn-danger'>Eliminar</button> "}
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "bDestroy": true,
            "createdRow": function (row, data, index) {
                if (data['N_USRRUT'] == -1) { //cuando no hay usuarios
                    $('#contenedorDataTable').hide();
                    document.getElementById("errorDataTable").textContent="El grupo de acceso no tiene usuarios";
                    $('#errorDataTable').show();
                } else {
                    $('#contenedorDataTable').show();
                    $('#errorDataTable').hide();
                }

            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return row['N_USRRUT'] + ' - ' + row['S_USRDIGITOVERIFICADOR'];
                    },
                    "targets": 0
                },
                {
                    "render": function (data, type, row) {
                        return row['S_USRPRIMERNOMBRE'] + '  ' + row['S_USRSEGUNDONOMBRE'] + '  ' + row['S_USRAPELLIDOPATERNO']
                            + '  ' + row['S_USRAPELLIDOMATERNO'];
                    },
                    "targets": 1
                }
            ]
        });
        resetearModalMADetalle();
        btnEliminarMantenedorAccesoDetalle('#listaUsuarios', dataTable)
    });
}

var rutRespuestaBuscarUsuarioMADetalle;

function buscarUsuarioMADetalle(formId) {
    if (!$(formId).validator('validate').has('.has-error').length) { //si todos los datos del form son validos
        event.preventDefault();
        var data = JSON.stringify($(formId).serializeArray());

        $.ajax({
            type: "GET",
            url: "../php/USRConsultas.php",
            data: {actionUSR: 'getDatosUsuarioByRut', formData: data},
            success: function (response) {
                if (response.localeCompare('El usuario no existe') == 0) {
                    $('#panelUsuario').show();
                    $('#usuarioEncontrado').hide();
                    $('#errorUsuario').show();
                } else {
                    response = JSON.parse(response);
                    rutRespuestaBuscarUsuarioMADetalle = response[0].N_USRRUT;
                    $('#rutResultado').val(response[0].N_USRRUT + '-' + response[0].S_USRDIGITOVERIFICADOR);
                    $('#nombreResultado').val(response[0].S_USRPRIMERNOMBRE + ' ' + response[0].S_USRSEGUNDONOMBRE + ' ' + response[0].S_USRAPELLIDOPATERNO
                                    + ' ' + response[0].S_USRAPELLIDOMATERNO);
                    $('#empresaResultado').val(response[0].S_EMPRAZONSOCIAL);
                    $('#panelUsuario').show();
                    $('#errorUsuario').hide();
                    $('#usuarioEncontrado').show();
                }

            }
        });
    }
}

function resetearModalMADetalle() {
    $('#añadirGrupoAccesoModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
        $('#panelUsuario').hide();
        $('#usuarioEncontrado').hide();
        $('#errorUsuario').hide();
    });
}

function btnAñadirResultadoBusqueda() {
    $.ajax({
        type: "POST",
        url: "../php/GPAConsultas.php",
        data: {actionGPA: 'asociarUsuarioGrupoAcceso', rut: rutRespuestaBuscarUsuarioMADetalle},
        success: function (response) {
            if (response.localeCompare('yaExiste') == 0) {
                usuarioYaExisteResultadoBusqueda();
            } else {
                $('#listaUsuarios').DataTable().ajax.reload();
                $('#añadirGrupoAccesoModal').modal('toggle');
            }

        }
    });
}

function usuarioYaExisteResultadoBusqueda() {
    bootbox.alert({
        message: "El usuario ya se encuentra anexado a este grupo de acceso",
        size: 'small',
        callback: function () {
            $('#añadirGrupoAccesoModal').modal('toggle');
        }
    })
}

function btnEliminarMantenedorAccesoDetalle(nombreDatatable, dataTable) {
    $(nombreDatatable).on('click', 'button.eliminar', function () {
        var data = dataTable.row($(this).parents("tr")).data();
        bootbox.confirm({
            title: "Eliminar usuario del grupo actual",
            message: "Esta seguro de eliminar a "+ data.S_USRPRIMERNOMBRE + ' ' + data.S_USRSEGUNDONOMBRE + ' ' +
                      data.S_USRAPELLIDOPATERNO + ' ' + data.S_USRAPELLIDOMATERNO +"?",
            buttons: {
                confirm: {
                    label: 'Confirmar',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result === true) {
                    $.ajax({
                        type: "POST",
                        url: "../php/GPAConsultas.php",
                        data: {actionGPA: 'eliminarUsuarioGrupoAcceso', "rut": data.N_USRRUT},
                        success: function (response) {
                            console.log(response);
                            $('#listaUsuarios').DataTable().ajax.reload();
                        }
                    });
                }

            }
        });
    });
}