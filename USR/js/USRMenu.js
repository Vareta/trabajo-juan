/**
 * Añade un menu padre, sin menus hijos (submenus), al elemento dropdown
 * @param nombre Nombre del menu
 * @param urlMenu Url del menu
 */
function añadir(nombre, urlMenu) {
    var parent = document.getElementById('dynamicNavBar');//obtiene el elemento al cual se va a anclar
    var newElement = document.createElement('li');
    newElement.innerHTML = '<a href="' + urlMenu + '">' + nombre + '</a>';
    parent.appendChild(newElement);
}

/**
 * Añade un menu hijo (submenu) a un menu padre
 * @param parent Elemento menu padre
 * @param nombre Nombre menu hijo
 * @param url Url menu hijo
 */
function addToParent(parent, nombre, url) {
    var element = document.createElement('li');
    element.innerHTML = '<a href="' + url + '">' + nombre + '</a>';
    parent.appendChild(element);
}

/**
 * Crea el elemento que contiene al menu padre y a los menus hijos (submenus)
 * @param menuPadre Elemento menu padre
 * @param menusHijos Arreglo que contiene todos los menus hijos del menu padre
 */
function añadirHijos(menuPadre, menusHijos) {
    var parent = document.getElementById('dynamicNavBar'); //obtiene el elemento al cual se va a anclar
    var dropdownElement = document.createElement('li'); //crea el elemento li
    dropdownElement.setAttribute('class', 'dropdown'); //setea la clase

    var dropdownToggleElement = document.createElement('a'); //crea un subelemento de la dropdown
    dropdownToggleElement.setAttribute('class', 'dropdown-toggle');
    dropdownToggleElement.setAttribute('data-toggle', 'dropdown');
    dropdownToggleElement.setAttribute('href', menuPadre.S_MNUURL); //setea el url del elemento padre de la lista
    var text = document.createTextNode(menuPadre.S_MNUNOMBRE); //setea el nombre del elemento padre de la lista
    dropdownToggleElement.append(text); //añade el nombre al subelemento

    var dropdownSpanElement = document.createElement('span'); //crea otro subelelemento
    dropdownSpanElement.setAttribute('class', 'caret');
    dropdownToggleElement.appendChild(dropdownSpanElement); //añade el subelemento

    var dropdownMenuElement = document.createElement('ul'); //crea el subelemento donde iran los menus hijos
    dropdownMenuElement.setAttribute('class', 'dropdown-menu'); //setea los atributos

    for (var i = 0; i < menusHijos.length; i++) {
        addToParent(dropdownMenuElement, menusHijos[i].S_MNUNOMBRE, menusHijos[i].S_MNUURL)
    }

    dropdownElement.appendChild(dropdownToggleElement); //añade el subelemento que contiene el menu padre
    dropdownElement.appendChild(dropdownMenuElement); //añade el subelemento que contiene los menu hijos

    parent.appendChild(dropdownElement); //añade el submenu al elemento ancla
}

/**
 * Verifica si un menu tiene submenus
 * @param menus Arreglo que contiene los menus del usuario
 * @param posicion Posicion en la que se encuentra el menu a consultar
 * @returns {boolean} Respuesta
 */
function tieneSubMenus(menus, posicion) {
    for (i = posicion + 1; i < menus.length; i++) {
        if (menus[posicion].N_MNUID == menus[i].USR_N_MNUID) {
            return true
        }
    }
    return false;
}

/**
 * Recopila todos los submenus de un menu padre
 * @param menus Arreglo que contiene los menus del usuario
 * @param posicion Posicion el la que se encuentra el menu padre del cual se requieren recopilar los submenus
 * @returns {Array} Arreglo con los menus hijos
 */
function recopilaSubMenus(menus, posicion) {
    var hijos = [];
    for (i = posicion + 1; i < menus.length; i++) {
        if (menus[posicion].N_MNUID == menus[i].USR_N_MNUID) {
            hijos.push(menus[i]);
        }
    }
    return hijos;
}

/**
 * Crea el menu dinamico para el usuario que se encuentra en la aplicacion
 * @param $appId ID de la aplicacion
 */
function crearMenu() {
    $.ajax({
        type: "GET",
        url: "../php/USRConsultas.php",
        data: {actionUSR: 'getMenuUsuario'},
        success: function (response) {
            var jsonResponse = JSON.parse(response);
            var contenido;
            for (var key in jsonResponse) {
                if (jsonResponse.hasOwnProperty(key)) {
                    contenido = jsonResponse[key];
                }
            }

            for (var i = 0; i < contenido.length; i++) {
                if (contenido[i].USR_N_MNUID == null && !tieneSubMenus(contenido, i)) {//si es menu padre y no tiene submenus
                    añadir(contenido[i].S_MNUNOMBRE, contenido[i].S_MNUURL);
                }
                if (contenido[i].USR_N_MNUID == null && tieneSubMenus(contenido, i)) {//si es menu padre y tiene submenus
                    añadirHijos(contenido[i], recopilaSubMenus(contenido, i));
                }
            }
        }
    });
}