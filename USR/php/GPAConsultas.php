<?php
if (!isset($_SESSION)) {
    session_start();
}
include '../../db/db.php';
/**
 * Recibe las acciones y deriva a las funciones correspondientes
 */
if (isset($_POST['actionGPA'])) {
    $action = $_POST['actionGPA'];
    switch ($action) {
        case 'añadirGrupoAcceso':
            añadirGrupoAcceso($_POST['formData']);
            break;
        case 'modificarGrupoAccesoDetalle':
            modificarGrupoAccesoDetalle($_POST['tableData']);
            break;
        case 'asociarSubMenuAGrupoAcceso':
            asociarSubMenuAGrupoAcceso($_POST['tableData']);
            break;
        case 'asociarUsuarioGrupoAcceso':
            asociarUsuarioGrupoAcceso($_POST['rut']);
            break;
        case 'eliminarUsuarioGrupoAcceso':
            eliminarUsuarioGrupoAcceso($_POST['rut']);
            break;
        case 'eliminarGrupoAcceso':
            eliminarGrupoAcceso($_POST['grupoAccesoId']);
            break;
        default:
            die('No existe tal función');
    }
}
if (isset($_GET['actionGPA'])) {
    $action = $_GET['actionGPA'];
    switch ($action) {
        case 'getUsuariosGrupoAcceso':
            getUsuariosGrupoAcceso();
            break;
        case 'getGrupoAccesoDefault':
            getGrupoAccesoDefault();
            break;
        case 'getGrupoAccesoByAppId':
            getGrupoAccesoByAppId($_GET['appId']);
            break;
        default:
            die('No existe tal función gpa');
    }
}

/**
 * Añade un grupo de acceso a la aplicacion desde la vista USRVGrupoAcceso.php
 * @param $data: Datos que vienen desde el formulario. Contiene el id de la aplicacion y el nombre del grupo de acceso
 */
function añadirGrupoAcceso($data) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $data = json_decode($data, true);
    $appId = mysqli_real_escape_string($conectar, $data[0]['value']);
    $grupoAccesoNombre = mysqli_real_escape_string($conectar, $data[1]['value']);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_grupoacceso (N_APPID, S_GPANOMBRE, D_GPAFECHACREACION) 
            VALUES ('$appId', '$grupoAccesoNombre', '$date')";
    $result = mysqli_query($conectar, $sql);
    mysqli_close($conectar);
    if ($result) {
        echo "exito";
    }
}

/**
 * Verifica que un menu este asociado a un grupo de acceso
 * @param $grupoAccesoId: ID del grupo de acceso
 * @param $appId: ID de la aplicacion
 * @param $menuId: ID del menu
 * @return bool: Booleano con la respuesta
 */
function estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $grpoAccesoId = mysqli_real_escape_string($conectar, $grupoAccesoId);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "SELECT N_GPASECUENCIAL FROM `usr_detallegrupoacceso` WHERE N_APPID = '$idApp' AND N_MNUID = '$idMenu' AND N_GPASECUENCIAL = '$grpoAccesoId'";
    $detalleGAQuery = mysqli_query($conectar, $sql);
    if ($detalleGAQuery) {
        while ($data = mysqli_fetch_assoc($detalleGAQuery)) {
            $contenido["data"][] = $data;
        }
        if (isset($contenido)) {
            mysqli_free_result($detalleGAQuery);
            mysqli_close($conectar);
            $respuesta = true;
        } else {
            $respuesta = false;
        }

    } else {
        $respuesta = false;
    }
    return $respuesta;
}

/**
 * Modifica los permisos de acceso de los menu pertenecientes al grupo de acceso
 * @param $data: Form que contiene los nuevos permisos para los menu
 */
function modificarGrupoAccesoDetalle($data) {
    $datos = json_decode($data, true);
    $appId = $_SESSION['appIdAux'];
    $grupoAccesoId = $_SESSION['grupoAccesoId'];
    for ($i = 0; $i < count($datos); $i++) {
        if (isset($datos[$i]['N_ESTAASIGNADO'])) {
            $deboAgregar = $datos[$i]['N_ESTAASIGNADO'];
            echo 'asignado  '.$deboAgregar.'<br>';
            $menuId = $datos[$i]['N_MNUID'];
            $estaAsociado = estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $menuId);
            if ($deboAgregar && !$estaAsociado) { //agregar
                echo 'debo agregar'.'<br>';
                asociarMenuAGrupoAcceso($grupoAccesoId, $appId, $menuId);
            } else if (!$deboAgregar && $estaAsociado) { //eliminar
                eliminarMenuDeGrupoAcceso($grupoAccesoId, $appId, $menuId);
                echo 'debo eliminar'.'<br>';
            } else {
                echo 'no hago nada'.'<br>';
            }

        } else {
            echo 'no asignado'.'<br>';
        }
    }
}

/**
 * Asocia un menu a un grupo de acceso
 * @param $grupoAccesoId: ID grupo acceso
 * @param $appId: ID de la aplicacion
 * @param $menuId: ID del menu
 * @return bool: Booleano con la respuesta
 */
function asociarMenuAGrupoAcceso($grupoAccesoId, $appId, $menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $grpoAccesoId = mysqli_real_escape_string($conectar, $grupoAccesoId);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_detallegrupoacceso (N_APPID, N_GPASECUENCIAL, N_MNUID, D_DGAFECHACREACION) 
            VALUES ('$idApp', '$grpoAccesoId', '$idMenu', '$date')";
    $result = mysqli_query($conectar, $sql);
    mysqli_close($conectar);
    if ($result) {
        return true;
    } else {
        return false;
    }
}

/**
 * Asocia un sub menu a un grupo de acceso
 * @param $data: form que contiene los datos a asociar del o de los sub menus
 */
function asociarSubMenuAGrupoAcceso($data) {
    $datos = json_decode($data, true);
    $appId = $_SESSION['appIdAux'];
    $grupoAccesoId = $_SESSION['grupoAccesoId'];
    for ($i = 0; $i < count($datos); $i++) {
        if (isset($datos[$i]['N_ESTAASIGNADO'])) {
            $deboAgregar = $datos[$i]['N_ESTAASIGNADO'];
            $menuId = $datos[$i]['N_MNUID'];
            $estaAsociado = estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $menuId);
            if ($deboAgregar && !$estaAsociado) { //agregar
                $menuPadreId = $datos[$i]['USR_N_MNUID'];
                if (!estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $menuPadreId)) { //si el menu padre no se encuentra en el grupo de acceso
                    asociarMenuAGrupoAcceso($grupoAccesoId, $appId, $menuPadreId); // se agrega el menu padre antes del submenu
                }
                asociarMenuAGrupoAcceso($grupoAccesoId, $appId, $menuId); //añade el sub menu
            } else if (!$deboAgregar && $estaAsociado) { //eliminar
                eliminarSubMenuDeGrupoAcceso($grupoAccesoId, $appId, $menuId);
            } else {
                //echo 'no hago nada'.'<br>';
            }

        }
    }
}

/** Elimina la asociacion de un sub menu con un grupo de acceso
 * @param $grupoAccesoId: ID del grupo de acceso
 * @param $appId: ID de la aplicacion
 * @param $menuId: ID del sub menu
 */
function eliminarSubMenuDeGrupoAcceso($grupoAccesoId, $appId, $menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $grpoAccesoId = mysqli_real_escape_string($conectar, $grupoAccesoId);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "DELETE FROM usr_detallegrupoacceso WHERE N_GPASECUENCIAL = '$grpoAccesoId' AND N_APPID = '$idApp' AND N_MNUID = '$idMenu'";
    $eliminarQuery = mysqli_query($conectar, $sql);

    if ($eliminarQuery) {
        echo "exito";
    } else {
        echo "error en eliminarMenuDeGrupoAcceso";
    }
}

/**
 * Elimina la asociacion de un menu del grupo de acceso. Si el menu tiene sub menus asociados, elimina la asociacion de
 * estos igualmente
 * @param $grupoAccesoId: ID del grupo de acceso
 * @param $appId: ID de la aplicacion
 * @param $menuId: ID del menu
 */
function eliminarMenuDeGrupoAcceso($grupoAccesoId, $appId, $menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "SELECT N_MNUID FROM usr_menu WHERE USR_N_MNUID = '$idMenu' "; //busca los submenus del menu padre
    $subMenusPadreQuery = mysqli_query($conectar, $sql);
    if ($subMenusPadreQuery) { //elimina los submenus del grupo de acceso
        while ($data = mysqli_fetch_assoc($subMenusPadreQuery)) {
            $subMenusPadre[] = $data;
        }
        if (isset($subMenusPadre)) {
            foreach($subMenusPadre as $submenu) {
                eliminarSubMenuDeGrupoAcceso($grupoAccesoId, $appId, $submenu['N_MNUID']);
            }
        }
    }
    eliminarSubMenuDeGrupoAcceso($grupoAccesoId, $appId, $menuId); //elimina el menu (despues de haber eliminado todos sus submenus)
}

/**
 *Obtiene todos los usuarios que tienen acceso al grupo de acceso
 */
function getUsuariosGrupoAcceso() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $grupoAccesoId = mysqli_real_escape_string($conectar, $_SESSION['grupoAccesoId']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appIdAux']);
    $sql = "SELECT usr_usuario.N_USRRUT, usr_usuario.S_USRDIGITOVERIFICADOR, usr_usuario.S_USRPRIMERNOMBRE, 
            usr_usuario.S_USRSEGUNDONOMBRE, usr_usuario.S_USRAPELLIDOPATERNO, usr_usuario.S_USRAPELLIDOMATERNO, 
            usr_grupoaccesousuarios.N_APPID, usr_grupoaccesousuarios.N_GPASECUENCIAL FROM usr_usuario INNER JOIN 
            usr_grupoaccesousuarios ON usr_usuario.N_USRRUT = usr_grupoaccesousuarios.N_USRRUT WHERE 
            usr_grupoaccesousuarios.N_APPID = '$appId' AND usr_grupoaccesousuarios.N_GPASECUENCIAL = '$grupoAccesoId' ";
    $usuariosGrupoAccesoQuery = mysqli_query($conectar, $sql);
    if ($usuariosGrupoAccesoQuery) {
        while ($data = mysqli_fetch_assoc($usuariosGrupoAccesoQuery)) {
            $usuariosGrupoAcceso["data"][] = $data;
        }
        if (isset($usuariosGrupoAcceso)) {
            mysqli_free_result($usuariosGrupoAccesoQuery);
            mysqli_close($conectar);
            echo json_encode($usuariosGrupoAcceso);
        } else {
            echo "{\"data\":[{\"N_USRRUT\":\"-1\",\"S_USRDIGITOVERIFICADOR\":\"-1\",\"S_USRPRIMERNOMBRE\":\"\",
                    \"S_USRSEGUNDONOMBRE\":\"\",\"S_USRAPELLIDOPATERNO\":\"\",\"S_USRAPELLIDOMATERNO\":\"\",
                    \"N_APPID\":\"\",\"N_GPASECUENCIAL\":\"\"}]}";
        }
    } else {
        echo "error en getUsuariosGrupoAcceso";
    }
}

/**
 * Asocia un usuario a un grupo de acceso
 * @param $rut: Rut (sin digito verificador) del usuario a asociar
 */
function asociarUsuarioGrupoAcceso($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appIdAux']);
    $grupoAccesoId = mysqli_real_escape_string($conectar, $_SESSION['grupoAccesoId']);

    $existeSql = "SELECT N_USRRUT FROM usr_grupoaccesousuarios WHERE N_USRRUT = '$usrRut' AND N_APPID = '$appId' AND N_GPASECUENCIAL = '$grupoAccesoId' ";
    $existeQuery = mysqli_query($conectar, $existeSql);
    if ($existeQuery) {
        while ($data = mysqli_fetch_assoc($existeQuery)) {
            $existe[] = $data;
        }
        if (isset($existe)) {
            mysqli_free_result($existeQuery);
            mysqli_close($conectar);
            echo "yaExiste";
        } else { //el usuario no esta anexado
            date_default_timezone_set('America/Santiago'); //establece la zona horaria
            $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
            $gauEstado = 1;
            $sql = "INSERT INTO usr_grupoaccesousuarios (N_USRRUT, N_APPID, N_GPASECUENCIAL, N_GAUESTADO, D_GAUFECHACREACION) 
                    VALUES ('$usrRut', '$appId', '$grupoAccesoId', '$gauEstado', '$date')";
            $añadirAGrupoQuery = mysqli_query($conectar, $sql);
            if ($añadirAGrupoQuery) {
                echo "exito";
            } else {
                echo "error añadiendo usuario";
            }
        }
    }
}

/**
 * Elimina a un usuario del grupo de acceso
 * @param $rut: Rut (sin digito verificador) del usuario a eliminar del grupo de acceso
 */
function eliminarUsuarioGrupoAcceso($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appIdAux']);
    $grupoAccesoId = mysqli_real_escape_string($conectar, $_SESSION['grupoAccesoId']);

    $sql = "DELETE FROM usr_grupoaccesousuarios WHERE N_GPASECUENCIAL = '$grupoAccesoId' AND N_APPID = '$appId' AND N_USRRUT = '$usrRut'";
    $eliminarQuery = mysqli_query($conectar, $sql);

    if ($eliminarQuery) {
        echo "exito";
    } else {
        echo "error en eliminarUsuarioGrupoAcceso";
    }
}

/**
 * Obtiene todos los grupos de acceso de la aplicacion actual
 */
function getGrupoAccesoDefault() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appId']);
    $sql = "SELECT usr_grupoacceso.S_GPANOMBRE, usr_grupoacceso.N_APPID, usr_grupoacceso.N_GPASECUENCIAL FROM usr_grupoacceso WHERE N_APPID = '$appId' ";
    $gruposAccesoUsuarioQuery = mysqli_query($conectar, $sql);
    while ($data = mysqli_fetch_assoc($gruposAccesoUsuarioQuery)) {
        $arregloGrupoAcceso["data"][] = $data;
    }

    if (isset($arregloGrupoAcceso)) {
        mysqli_free_result($gruposAccesoUsuarioQuery);
        mysqli_close($conectar);
        echo json_encode($arregloGrupoAcceso);
    } else {
        echo "{\"data\":[{\"S_GPANOMBRE\":\"-1\",\"N_APPID\":\"-1\",\"N_GPASECUENCIAL\":\"-1\"}]}"; //para cuando no existen grupos de acceso
    }
}

/**
 * Obtiene todos los grupos de acceso para una aplicacion dada
 * @param $appId: ID de la aplicacion
 */
function getGrupoAccesoByAppId($appId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $sql = "SELECT usr_grupoacceso.S_GPANOMBRE, usr_grupoacceso.N_APPID, usr_grupoacceso.N_GPASECUENCIAL FROM usr_grupoacceso WHERE N_APPID = '$idApp'";
    $gruposAccesoUsuarioQuery = mysqli_query($conectar, $sql);
    while ($data = mysqli_fetch_assoc($gruposAccesoUsuarioQuery)) {
        $arregloGrupoAcceso["data"][] = $data;
    }

    if (isset($arregloGrupoAcceso)) {
        mysqli_free_result($gruposAccesoUsuarioQuery);
        mysqli_close($conectar);
        echo json_encode($arregloGrupoAcceso);
    } else {
        echo "{\"data\":[{\"S_GPANOMBRE\":\"-1\",\"N_APPID\":\"-1\",\"N_GPASECUENCIAL\":\"-1\"}]}"; //para cuando no existen grupos de acceso
    }
    include_once 'USRLogConsultas.php';
    añadirLogAplicacion($appId, $_SESSION['usrRut']); //registra la aplicacion en el log
}

/**
 * Elimina un grupo de acceso y todo lo relacionado a el, mediante un procedimiento almacenado
 * @param $grupoAccesoId: ID del grupo de acceso
 */
function eliminarGrupoAcceso($grupoAccesoId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idGrupoAcceso = mysqli_real_escape_string($conectar, $grupoAccesoId);
    $sql = "CALL eliminar_grupo_acceso('$idGrupoAcceso')";
    $procedureQuery = mysqli_query($conectar, $sql);
    if ($procedureQuery) {
        echo "exito";
    }
}