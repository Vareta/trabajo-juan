<?php
if (!isset($_SESSION)) {
    session_start();
}
include 'C:\Users\Vareta\Desktop\Trabajo php juan\Proyecto\db\db.php';
/**
 * Recibe las acciones y deriva a las funciones correspondientes
 */
if (isset($_GET['actionUSR'])) {
    $action = $_GET['actionUSR'];
    switch ($action) {
        case 'getMenuUsuario':
            getMenuUsuario();
            break;
        case 'getUsuariosAplicacion':
            getUsuariosAplicacion();
            break;
        case 'getGruposAccesoUsuario':
            getGruposAccesoUsuario($_GET['rut']);
            break;
        case 'getAplicacionesUsuario':
            getAplicacionesUsuario();
            break;
        case 'getDatosUsuarioByRut':
            getDatosUsuarioByRut($_GET['formData']);
            break;
        default:
            die('No existe tal función');
    }
}

if (isset($_POST['actionUSR'])) {
    $action = $_POST['actionUSR'];
    switch ($action) {
        case 'editarUsuario':
            editarUsuario($_POST['formData']);
            break;
        case 'añadirUsuario':
            añadirUsuario($_POST['formData']);
            break;
        case 'eliminarUsuario':
            eliminarUsuario($_POST['rut']);
            break;
        default:
            die('No existe tal función');
    }
}

/**
 * Comprueba si el usuario tiene menus asignados
 * @param $usrRut: Rut del usuario
 * @param $appId: ID de la aplicacion
 * @return bool: Respuesta
 */
function usuarioTieneMenu($usrRut, $appId) {
    $respuesta = true;
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $sql = "SELECT N_USRRUT FROM usr_grupoaccesousuarios WHERE N_USRRUT = '$usrRut' AND N_APPID = '$appId' ";
    $result = mysqli_query($conectar, $sql);

    if (!$result || mysqli_num_rows($result) == 0) { //no existen resultados
        $respuesta = false;
    }

    mysqli_free_result($result);
    mysqli_close($conectar);
    return $respuesta;
}

/**
 * Obtiene todos los menus asociados al usuarios para cierta aplicacion y los envia en formato Json
 * @param $appId: ID de la aplicacion
 */
function getMenuUsuario() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appId']);
    $sql = "SELECT DISTINCT usr_menu.N_MNUID, usr_menu.USR_N_MNUID, usr_menu.S_MNUNOMBRE, usr_menu.S_MNUURL FROM usr_detallegrupoacceso
            INNER JOIN usr_grupoaccesousuarios ON usr_detallegrupoacceso.N_GPASECUENCIAL = usr_grupoaccesousuarios.N_GPASECUENCIAL 
            INNER JOIN usr_menu ON usr_detallegrupoacceso.N_MNUID = usr_menu.N_MNUID 
            WHERE usr_grupoaccesousuarios.N_USRRUT = '$usrRut' AND usr_grupoaccesousuarios.N_APPID = '$appId'";
    $result = mysqli_query($conectar, $sql);
    if ($result) {
        while ($data = mysqli_fetch_assoc($result)) {
            $contenido["data"][] = $data;
        }
        mysqli_free_result($result);
        mysqli_close($conectar);
        echo json_encode($contenido);
    } else {
        echo "error consiguiendo getMenu";
    }
}

/**
 * Añade un nuevo usuario, el cual estará asociado a la empresa que tenga el usuario que lo añade
 * @param $data: Form que contiene los datos del nuevo usuario
 *
 */
function añadirUsuario($data) {
    include_once '../../code/utils.php';
    //se añade mysqli_real_escape_string para prevenir inyecciones sql
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $datos = json_decode($data, true);
    $rut =  mysqli_real_escape_string($conectar, $datos[0]['value']);
    $primerNombre = mysqli_real_escape_string($conectar, $datos[1]['value']);
    $segundoNombre = mysqli_real_escape_string($conectar, $datos[2]['value']);
    $apellidoPaterno = mysqli_real_escape_string($conectar, $datos[3]['value']);
    $apellidoMaterno = mysqli_real_escape_string($conectar, $datos[4]['value']);
    $mail = mysqli_real_escape_string($conectar, $datos[5]['value']);
    $password = mysqli_real_escape_string($conectar, $datos[6]['value']);
    $empRut = intval(mysqli_real_escape_string($conectar, $_SESSION['empRut']));

    $rutAux = preg_split("/-/", $rut); //separa el rut
    if (!validaRutCompleto($rut)) { //rut invalido
        echo "rutUsrInvalido";
    } else if (existeUsuario(intval($rutAux[0]))) { //Ya existe el usuario
        echo "existeUsuario";
    } else {
        $hashPassword = password_hash($password, PASSWORD_DEFAULT);
        date_default_timezone_set('America/Santiago'); //establece la zona horaria
        $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
        $rutSinDigitoVerificador = intval($rutAux[0]);
        $sql = "INSERT INTO usr_usuario (N_USRRUT, N_EMPRUT, S_USRDIGITOVERIFICADOR, S_USRCONTRASENA, S_USREMAIL, S_USRPRIMERNOMBRE,
            S_USRSEGUNDONOMBRE, S_USRAPELLIDOPATERNO, S_USRAPELLIDOMATERNO, D_USRFECHACREACION) 
            VALUES ('$rutSinDigitoVerificador', '$empRut', '$rutAux[1]', '$hashPassword', '$mail', '$primerNombre', '$segundoNombre',
            '$apellidoPaterno', '$apellidoMaterno', '$date')";
        $añadirUsuarioQuery = mysqli_query($conectar, $sql);

        if ($añadirUsuarioQuery) {
            include_once 'USRLogConsultas.php';
            añadirUsuarioLog($rutSinDigitoVerificador, $date); //guarda un registro de cuando se añade un usuario
            echo "usuarioAñadido";
        } else {
            echo "error en añadirUsuario";
        }
        mysqli_free_result($añadirUsuarioQuery);
        mysqli_close($conectar);

    }
}

/**
 * Obtiene todos los usuarios que pertenezcan a las aplicaciones a las cuales el "Usuario logueado" esta asociado
 */
function getUsuariosAplicacion() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $sqlAplicaciones = "SELECT DISTINCT N_APPID FROM usr_grupoaccesousuarios WHERE N_USRRUT = '$usrRut' ";
    $idAplicaciones = mysqli_query($conectar, $sqlAplicaciones);
    while ($data = mysqli_fetch_assoc($idAplicaciones)) {
        $arregloAppIdp[] = $data;
    }
    if (isset($arregloAppIdp)) {
        foreach ($arregloAppIdp as $id) {
            $idAux = $id['N_APPID'];
            $sql = "SELECT DISTINCT usr_usuario.N_USRRUT, usr_usuario.S_USRDIGITOVERIFICADOR, usr_usuario.S_USREMAIL, usr_usuario.S_USRPRIMERNOMBRE,
            usr_usuario.S_USRSEGUNDONOMBRE, usr_usuario.S_USRAPELLIDOPATERNO, usr_usuario.S_USRAPELLIDOMATERNO FROM usr_usuario
            INNER JOIN usr_grupoaccesousuarios ON usr_usuario.N_USRRUT = usr_grupoaccesousuarios.N_USRRUT WHERE N_APPID = '$idAux'";
            $usuarioApplicacion = mysqli_query($conectar, $sql);
            if ($usuarioApplicacion) {
                while ($data = mysqli_fetch_assoc($usuarioApplicacion)) {
                    $contenido[] = $data;
                }
                mysqli_free_result($usuarioApplicacion);
            }

        }
        mysqli_free_result($idAplicaciones);
        mysqli_close($conectar);
        if (isset($contenido)) {
            $details["data"] = unique_multidim_array($contenido,'N_USRRUT');
            echo json_encode($details);
        } else {
            echo "error consiguiendo getUsuariosAplicacion";
        }

    } else {
        echo "Usuario no tiene aplicaciones";
    }

}

/**
 * Elimina valores repetidos de cierta columna en un array. Funcion sacada de:
 * "http://php.net/manual/es/function.array-unique.php"
 * @param $array: Arreglo que contiene los valores
 * @param $key: Nombre de la columna
 * @return array: Arreglo sin valores repetidos
 */
function unique_multidim_array($array, $key) {
    $temp_array = array();
    $i = 0;
    $key_array = array();

    foreach($array as $val) {
        if (!in_array($val[$key], $key_array)) {
            $key_array[$i] = $val[$key];
            $temp_array[$i] = $val;
        }
        $i++;
    }
    return $temp_array;
}


/**
 * Obtiene los grupos de acceso a los cuales esta asociado cierto usuario
 * @param $rut: Rut (sin digito verificador) del usuario a consultar
 */
function getGruposAccesoUsuario($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appId']);
    $sqlGruposAcceso = "SELECT usr_grupoacceso.N_APPID, usr_grupoacceso.S_GPANOMBRE FROM usr_grupoacceso INNER JOIN usr_grupoaccesousuarios 
                        ON usr_grupoacceso.N_GPASECUENCIAL = usr_grupoaccesousuarios.N_GPASECUENCIAL WHERE N_USRRUT = '$usrRut'
                         AND usr_grupoaccesousuarios.N_APPID = '$appId' ";
    $grupoAccesoQuery = mysqli_query($conectar, $sqlGruposAcceso);
    while ($data = mysqli_fetch_assoc($grupoAccesoQuery)) {
        $arregloGrupoAccesoNombre["data"][] = $data;
    }
    if (isset($arregloGrupoAccesoNombre)) {
        for ($i = 0; $i < count($arregloGrupoAccesoNombre["data"]); $i++) {
            $idAppAux = $arregloGrupoAccesoNombre["data"][$i]['N_APPID'];
            $sqlNombreApp = "SELECT S_APPNOMBRE FROM `usr_aplicacion` WHERE N_APPID = '$idAppAux' ";
            $nombreAppQuery = mysqli_query($conectar, $sqlNombreApp);
            if ($nombreAppQuery) {
                while ($data = mysqli_fetch_assoc($nombreAppQuery)) {
                    $arregloGrupoAccesoNombre["data"][$i]['S_APPNOMBRE'] = $data['S_APPNOMBRE'];
                }
                mysqli_free_result($nombreAppQuery);
            }
        }
        mysqli_free_result($grupoAccesoQuery);
        mysqli_close($conectar);
        if (isset($arregloGrupoAccesoNombre)) {
            echo json_encode($arregloGrupoAccesoNombre);
        } else {
            echo "error consiguiendo getGruposAccesoUsuario";
        }
    } else {
        echo "Usuario no tiene grupos asociados";
    }
}

/**
 * Edita los datos de un usuario (menos su rut)
 * @param $data: Datos del form, que contienen los datos modificar
 */
function editarUsuario($data) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $datos = json_decode($data, true);
    $rut = mysqli_real_escape_string($conectar, $datos[0]['value']);
    $primerNombre = mysqli_real_escape_string($conectar, $datos[1]['value']);
    $segundoNombre = mysqli_real_escape_string($conectar, $datos[2]['value']);
    $apellidoPaterno = mysqli_real_escape_string($conectar, $datos[3]['value']);
    $apellidoMaterno = mysqli_real_escape_string($conectar, $datos[4]['value']);
    $email = mysqli_real_escape_string($conectar, $datos[5]['value']);
    $password = mysqli_real_escape_string($conectar, $datos[6]['value']);
    $rutAux = preg_split("/-/", $rut); //separa el rut
    $hashPassword = password_hash($password, PASSWORD_DEFAULT);
    $sql = "UPDATE usr_usuario SET S_USRCONTRASENA = '$hashPassword', S_USREMAIL = '$email', S_USRPRIMERNOMBRE = '$primerNombre',
            S_USRSEGUNDONOMBRE = '$segundoNombre', S_USRAPELLIDOPATERNO = '$apellidoPaterno', S_USRAPELLIDOMATERNO = '$apellidoMaterno'
            WHERE N_USRRUT = '$rutAux[0]' ";
    $editarQuery = mysqli_query($conectar, $sql);

    if ($editarQuery) {
        echo "exito";
    } else {
        echo "error en editarUsuario";
    }
}

/**
 * Obtiene el rut de la empresa a la cual esta asociado el usuario
 * @param $rut: Rut del usuario, sin el digito verificador
 * @return mixed
 */
function getRutEmpresaUsuario($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $sql = "SELECT N_EMPRUT FROM usr_usuario WHERE N_USRRUT = '$usrRut' ";
    $result = mysqli_query($conectar, $sql);
    $rutEmpresa = mysqli_fetch_assoc($result)['N_EMPRUT'];

    mysqli_free_result($result);
    mysqli_close($conectar);

    return $rutEmpresa;
}

/**
 * Consulta si es que acaso existe otro usuario con el mismo rut
 * @param $rut: Rut del usuario, sin el digito verificador
 * @return bool Retorna respuesta mediante un boolean
 */
function existeUsuario($rut) {
    $respuesta = true;
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $sql = "SELECT N_USRRUT FROM usr_usuario WHERE N_USRRUT = '$usrRut' ";
    $result = mysqli_query($conectar, $sql);

    if (!$result || mysqli_num_rows($result) == 0) { //no existen resultados
        $respuesta = false;
    }
    mysqli_free_result($result);
    mysqli_close($conectar);
    return $respuesta;
}

/**
 * Obtiene la password del usuario (en este punto, el usuario siempre existe)
 * @param $rut Rut del usuario, sin el digito verificador
 * @return mixed Retorna la contraseña
 */
function getPasswordUsuario($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $sql = "SELECT S_USRCONTRASENA FROM usr_usuario WHERE N_USRRUT = '$usrRut' ";
    $result = mysqli_query($conectar, $sql);
    $password = mysqli_fetch_assoc($result)['S_USRCONTRASENA'];

    mysqli_free_result($result);
    mysqli_close($conectar);

    return $password;
}

/**
 * Obtiene los datos del usuario que esta actualmente logueado
 * @return bool|string
 */
function getDatosUsuario() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $sql = "SELECT N_USRRUT, S_USRDIGITOVERIFICADOR, S_USRPRIMERNOMBRE, S_USRSEGUNDONOMBRE, S_USRAPELLIDOPATERNO, S_USRAPELLIDOMATERNO FROM `usr_usuario` WHERE N_USRRUT = '$usrRut' ";
    $datosUsuarioQuery = mysqli_query($conectar, $sql);
    if ($datosUsuarioQuery) {
        $datosUsuario = mysqli_fetch_assoc($datosUsuarioQuery);
    }
    if (isset($datosUsuario)) {
        mysqli_free_result($datosUsuarioQuery);
        mysqli_close($conectar);
        return json_encode($datosUsuario);
    } else {
        echo "error consiguiendo getDatosUsuario";
    }

    return false; //utilizada sólo por el warning, si se llega a esta sentencia, no se pudieron conseguir los datos de usuario
}


/**
 * Obtiene las aplicaciones a las cuales el usuario tiene acceso
 */
function getAplicacionesUsuario() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $sql = "SELECT DISTINCT N_APPID FROM `usr_grupoaccesousuarios` WHERE N_USRRUT = '$usrRut' ";
    $aplicacionesIdQuery = mysqli_query($conectar, $sql);
    while ($data = mysqli_fetch_assoc($aplicacionesIdQuery)) {
        $arregloAplicacionesId[] = $data;
    }
    if (isset($arregloAplicacionesId)) {
        $contador = 0;
        foreach ($arregloAplicacionesId as $idApp) {
            $empRut = mysqli_real_escape_string($conectar, $_SESSION['empRut']);
            $idAppAux = $idApp['N_APPID'];
            $sqlAppIdNombre = "SELECT N_APPID, S_APPNOMBRE FROM `usr_aplicacion` WHERE N_APPID = '$idAppAux' AND N_EMPRUT = '$empRut' ";
            $appIdNombre = mysqli_query($conectar, $sqlAppIdNombre);
            if ($appIdNombre) {
                while ($data = mysqli_fetch_assoc($appIdNombre)) {
                    $arregloApppIdNombre[$contador] = $data;
                    $contador++;
                }
                mysqli_free_result($appIdNombre);
            }
        }
        mysqli_free_result($aplicacionesIdQuery);
        mysqli_close($conectar);
        if (isset($arregloApppIdNombre)) {
            echo json_encode($arregloApppIdNombre);
        } else {
            echo "Usuario no tiene aplicaciones";
        }
    } else {
        echo "error consiguiendo getAplicacionesUsuario";
    }
}


/**
 * Obtiene los datos de un usuario por su rut
 * @param $data: Datos del form donde se encuentra el rut
 */
function getDatosUsuarioByRut($data) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $datos = json_decode($data, true);
    $rut = mysqli_real_escape_string($conectar, $datos[0]['value']);
    $rutAux = preg_split("/-/", $rut);
    if (!existeUsuario($rutAux[0])){
        echo "El usuario no existe";
    } else {
        $sql = "SELECT N_USRRUT, S_USRDIGITOVERIFICADOR, S_USRPRIMERNOMBRE, S_USRSEGUNDONOMBRE, S_USRAPELLIDOPATERNO,
                S_USRAPELLIDOMATERNO, N_EMPRUT FROM usr_usuario WHERE N_USRRUT = '$rutAux[0]' ";
        $usuarioQuery = mysqli_query($conectar, $sql);
        while ($data = mysqli_fetch_assoc($usuarioQuery)) {
            $usuario[] = $data;
        }
        $rutEmp = $usuario[0]['N_EMPRUT'];
        $sqlEmp = "SELECT S_EMPRAZONSOCIAL FROM cmn_empresa WHERE N_EMPRUT = '$rutEmp' ";
        $nombreEmpQuery = mysqli_query($conectar, $sqlEmp);
        $nombreEmp = mysqli_fetch_assoc($nombreEmpQuery);

        $usuario[0]['S_EMPRAZONSOCIAL'] = $nombreEmp['S_EMPRAZONSOCIAL'];

        mysqli_free_result($usuarioQuery);
        mysqli_free_result($nombreEmpQuery);
        mysqli_close($conectar);
        echo json_encode($usuario);
    }
}



/**
 * Elimina un usuario y todo lo asociado a el, mediante un procedimiento almacenado
 * @param $rut:rut del usuario
 */
function eliminarUsuario($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    $sql = "CALL eliminar_usuario('$usrRut')";
    $procedureQuery = mysqli_query($conectar, $sql);
    if ($procedureQuery) {
        echo "exito";
    }
}

