<?php
include '../../db/db.php';

/**
 * Añade a un usuario a la tabla logbackend
 * Esta acción sucede cuando un usuario accede al mantenedor
 * @param $rut: Rut (sin digito verificador) del usuario
 */
function añadirUsuarioLog($rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $rutUsuario = mysqli_real_escape_string($conectar, $rut);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_logbackend (N_USRRUT, D_LBEFECHA) VALUES ('$rutUsuario', '$date' )";
    $añadirUsuarioLogQuery = mysqli_query($conectar, $sql);
    if ($añadirUsuarioLogQuery) {
       // echo "usuarioAñadido a log";
    } else {
        //echo "error en añadirUsuarioLog";
    }
    mysqli_close($conectar);
}

/**
 * Añade un registro a la tabla usr_logaplicacion
 * Esta accion sucede cada vez que el usuario accede a una aplicacion
 * @param $appId: ID de la aplicacion
 * @param $usrRut: Rut (sin digito verificador) del usuario
 */
function añadirLogAplicacion($appId, $usrRut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $rutUsuario = mysqli_real_escape_string($conectar, $usrRut);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_logaplicacion (N_APPID, N_USRRUT, D_LAPFECHA) VALUES ('$idApp', '$rutUsuario', '$date' )";
    $añadirLogAplicacionQuery = mysqli_query($conectar, $sql);
    if ($añadirLogAplicacionQuery) {
        // echo "aplicacionAñadido a log";
    } else {
        //echo "error en añadirLogAplicacion";
    }
    mysqli_close($conectar);
}


/**
 * Añade un registro a la tabla de usr_logmenu
 * Esta accion sucede cada vez que el usuario accede a una vista de menu
 * @param $mnuId: ID del menu al cual se accesa
 * @param $rut: Rut (sin digido verificador) del usuario
 */
function añadirLogMenu($mnuId, $rut) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $menuId = mysqli_real_escape_string($conectar, $mnuId);
    $usrRut = mysqli_real_escape_string($conectar, $rut);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_logmenu (N_USRRUT, N_MNUID, D_LMEFECHA) VALUES ('$usrRut', '$menuId', '$date' )";
    $añadirLogAplicacionQuery = mysqli_query($conectar, $sql);
    if ($añadirLogAplicacionQuery) {
        // echo "aplicacionAñadido a log";
    } else {
        //echo "error en añadirLogAplicacion";
    }
    mysqli_close($conectar);
}