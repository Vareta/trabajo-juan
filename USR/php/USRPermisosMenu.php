<?php
if (!isset($_SESSION)) {
    session_start();
}
include '../../db/db.php';

/**
 * Verifica si el usuario tiene acceso a un menu en especifico, segun la id dada
 * @param $menuId; ID del menu a consultar
 * @return bool: Booleano, con la respuesta
 */
function usrTieneAccesoAMenuActual($menuId) {
    $respuesta = false;
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appId']);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "SELECT usr_menu.S_MNUNOMBRE FROM usr_detallegrupoacceso
            INNER JOIN usr_grupoaccesousuarios ON usr_detallegrupoacceso.N_GPASECUENCIAL = usr_grupoaccesousuarios.N_GPASECUENCIAL 
            INNER JOIN usr_menu ON usr_detallegrupoacceso.N_MNUID = usr_menu.N_MNUID 
            WHERE usr_grupoaccesousuarios.N_USRRUT = '$usrRut' AND usr_grupoaccesousuarios.N_APPID = '$appId' AND usr_detallegrupoacceso.N_MNUID = '$idMenu' ";
    $tienePermisoSql = mysqli_query($conectar, $sql);
    if ($tienePermisoSql) {
        while ($data = mysqli_fetch_assoc($tienePermisoSql)) {
            $tienePermiso[] = $data;
        }
        if (isset($tienePermiso)) {
            $respuesta = true;
        }
        mysqli_free_result($tienePermisoSql);
        mysqli_close($conectar);
    } else {
        //echo "error consiguiendo usrTieneAccesoAMenuActual";
    }

    return $respuesta;
}


/**
 * Verifica si un usuario tiene permitido acceder a un grupo de acceso para añadir menus, en la vista USRVGrupoAccesoMenu.php
 * Tambien es utilizado para ver si un usuario tiene permitido acceder a la vista USRVMantenedorAccesoDetalle.php
 * @param $grpoAccesoId: id del grupo de acceso
 * @param $appId: id de la aplicacion a la cual pertenece el grupo de acceso
 * @return bool: Booleano con la respuesta
 */
function usrTieneAccesoAGrupoAcceso($grpoAccesoId, $appId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $rut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $empRut = mysqli_real_escape_string($conectar, $_SESSION['empRut']);
    $grupoAccesoId = mysqli_real_escape_string($conectar, $grpoAccesoId);
    $aplicacionId = mysqli_real_escape_string($conectar, $appId);
    $sql = "SELECT usr_grupoacceso.N_APPID FROM usr_grupoacceso INNER JOIN usr_grupoaccesousuarios ON usr_grupoacceso.N_APPID = 
            usr_grupoaccesousuarios.N_APPID INNER JOIN usr_aplicacion ON usr_grupoacceso.N_APPID = usr_aplicacion.N_APPID WHERE
             usr_grupoaccesousuarios.N_USRRUT = '$rut' AND usr_grupoaccesousuarios.N_APPID = '$aplicacionId' AND 
             usr_grupoacceso.N_GPASECUENCIAL = '$grupoAccesoId' AND usr_aplicacion.N_EMPRUT = '$empRut' ";
    $tieneAccesoQuery = mysqli_query($conectar, $sql);
    if ($tieneAccesoQuery) {
        while ($data = mysqli_fetch_assoc($tieneAccesoQuery)) {
            $acceso[] = $data;
        }
        if (isset($acceso)) {
            $resultado = true;

        } else {
            $resultado = false;
        }
        mysqli_free_result($tieneAccesoQuery);
    } else {
        echo "error en usrTieneAccesoAGrupoAcceso";
        $resultado = false;
    }

    mysqli_close($conectar);
    return $resultado;
}