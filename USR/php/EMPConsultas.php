<?php
if(!isset($_SESSION)) {
    session_start();
}
include '../../db/db.php';


if(isset($_GET['actionEMP'])) {
    $action = $_GET['actionEMP'];
    switch($action) {
        case 'getInfoAplicaciones':
            getInfoAplicaciones();
            break;
        default:
            die('No existe tal función');
    }
}

/**
 * Consigue la url del logo de la empresa a la cual pertenece el usuario
 * @return bool|string
 */
function getUrlLogoEmpresa() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $empRut = mysqli_real_escape_string($conectar, $_SESSION['empRut']);
    $sql = "SELECT S_EMPURLLOGO FROM `cmn_empresa` WHERE N_EMPRUT = '$empRut' ";
    $datosUsuarioQuery = mysqli_query($conectar, $sql);
    if ($datosUsuarioQuery) {
        $datosUsuario = mysqli_fetch_assoc($datosUsuarioQuery);
        if (isset($datosUsuario)) {
            mysqli_free_result($datosUsuarioQuery);
            mysqli_close($conectar);
            return json_encode($datosUsuario);
        } else {
            echo "empresa no tiene logo asignado";
        }
    } else {
        echo "error consiguiendo getUrlLogoEmpresa";
    }
    return false; //utilizada sólo por el warning, si se llega a esta sentencia, no se pudo conseguir la url del logo empresa
}

/**
 * Consigue todas las aplicaciones a las cuales el usuario tiene acceso
 */
function getInfoAplicaciones() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $usrRut = mysqli_real_escape_string($conectar, $_SESSION['usrRut']);
    $sqlAplicaciones = "SELECT DISTINCT usr_aplicacion.N_APPID, usr_aplicacion.S_APPNOMBRE, usr_aplicacion.S_APPURLSPAGINAINCIO,
                        usr_aplicacion.S_APPURLICONO FROM usr_aplicacion INNER JOIN usr_grupoaccesousuarios ON 
                        usr_aplicacion.N_APPID = usr_grupoaccesousuarios.N_APPID WHERE usr_grupoaccesousuarios.N_USRRUT = '$usrRut'";
    $idAplicacionesQuery = mysqli_query($conectar, $sqlAplicaciones);
    if ($idAplicacionesQuery) {
        while ($data = mysqli_fetch_assoc($idAplicacionesQuery)) {
            $arregloAppIdp["data"][] = $data;
        }
        if (isset($arregloAppIdp)) {
            mysqli_free_result($idAplicacionesQuery);
            mysqli_close($conectar);
            echo json_encode($arregloAppIdp);
        } else {
            echo "Usuario no tiene aplicaciones";
        }
    } else {
        echo "error consiguiendo getInfoAplicaciones";
    }
}