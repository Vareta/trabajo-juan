<?php
if (!isset($_SESSION)) {
    session_start();
}
include '../../db/db.php';

if (isset($_GET['actionMNU'])) {
    $action = $_GET['actionMNU'];
    switch ($action) {
        case 'getSubMenusDeMenuPadre':
            getSubMenusDeMenuPadre($_GET['menuId']);
            break;
        case 'getMenuByappId':
            getMenuByappId();
            break;
        case 'getSubMenusDataTableGrupoAccesoMenu':
            getSubMenusDataTableGrupoAccesoMenu($_GET['menuId']);
            break;
        case 'getMenuPadreByApp':
            getMenuPadreByApp();
            break;
        case 'getMenuPadreByAppOtorgada':
            getMenuPadreByAppOtorgada($_GET['appId']);
            break;
        default:
            die('No existe tal función');
    }
}

if (isset($_POST['actionMNU'])) {
    $action = $_POST['actionMNU'];
    switch ($action) {
        case 'añadirSubmenu':
            añadirSubmenu($_POST['formData']);
            break;
        case 'añadirMenu':
            añadirMenu($_POST['formData']);
            break;
        case 'eliminarMenu':
            eliminarMenu($_POST['menuId']);
            break;
        default:
            die('No existe tal función');
    }
}

/**
 * Consigue los sub menus asociados a un menu padre
 * @param $menuId: ID del menu padre
 */
function getSubMenusDeMenuPadre($menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "SELECT S_MNUNOMBRE FROM usr_menu WHERE USR_N_MNUID = '$idMenu' ";
    $subMenusPadreQuery = mysqli_query($conectar, $sql);
    if ($subMenusPadreQuery) {
        while ($data = mysqli_fetch_assoc($subMenusPadreQuery)) {
            $subMenusPadre[] = $data;
        }
        if (isset($subMenusPadre)) {
            mysqli_free_result($subMenusPadreQuery);
            mysqli_close($conectar);
            echo json_encode($subMenusPadre);
        } else {
            echo "[{\"S_MNUNOSUBMENU\":\"No existen sub menus asociados\"}]";
        }
    } else {
        echo "Error consiguiendo getSubMenusDeMenuPadre";
    }
}

/**
 * Crea un sub menu para un menu
 * @param $data: Datos del form necesarios para crear el sub menu
 */
function añadirSubMenu($data) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $data = json_decode($data, true);
    $menuPadreId = mysqli_real_escape_string($conectar, $data[0]['value']);
    $appId = mysqli_real_escape_string($conectar, $data[1]['value']);
    $subMenuNombre = mysqli_real_escape_string($conectar, $data[2]['value']);
    $subMenuUrl = mysqli_real_escape_string($conectar, $data[3]['value']);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_menu (USR_N_MNUID, N_APPID, S_MNUNOMBRE, S_MNUURL, D_MNUFECHACREACION) 
            VALUES ('$menuPadreId', '$appId', '$subMenuNombre', '$subMenuUrl', '$date')";
    $result = mysqli_query($conectar, $sql);
    mysqli_close($conectar);
    if ($result) {
        echo "exito";
    }
}

/**
 * Crea un menu para la aplicacion
 * @param $data: Datos del form necesarios para crear el menu
 */
function añadirMenu($data) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $data = json_decode($data, true);
    $appId = mysqli_real_escape_string($conectar, $data[0]['value']);
    $menuNombre = mysqli_real_escape_string($conectar, $data[1]['value']);
    $menuUrl = mysqli_real_escape_string($conectar, $data[2]['value']);
    date_default_timezone_set('America/Santiago'); //establece la zona horaria
    $date = date('Y-m-d H:i:s'); //obtiene la fecha y la hora
    $sql = "INSERT INTO usr_menu (N_APPID, S_MNUNOMBRE, S_MNUURL, D_MNUFECHACREACION) 
            VALUES ('$appId', '$menuNombre', '$menuUrl', '$date')";
    $result = mysqli_query($conectar, $sql);
    mysqli_close($conectar);
    if ($result) {
        echo "exito";
    }
}

/**
 * Consigue todos los menu padre de una aplicacion para ser mostrados en una datatable
 */
function getMenuByappId() {
    include_once '../php/GPAConsultas.php';
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appIdAux']);
    $sql = "SELECT N_MNUID, S_MNUNOMBRE FROM usr_menu WHERE N_APPID = '$appId' AND USR_N_MNUID IS NULL ";
    $menusPadreQuery = mysqli_query($conectar, $sql);
    if ($menusPadreQuery) {
        $i = 0;
        $grupoAccesoId = mysqli_real_escape_string($conectar, $_SESSION['grupoAccesoId']);
        while ($data = mysqli_fetch_assoc($menusPadreQuery)) {
            $menusPadre["data"][$i] = [
                'N_ESTAASIGNADO' => estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $data["N_MNUID"]),
                'N_MNUID' => $data["N_MNUID"],
                'S_MNUNOMBRE' => utf8_encode($data["S_MNUNOMBRE"])
            ];
            $i++;
        }
    }

    if (isset($menusPadre)) {
        mysqli_free_result($menusPadreQuery);
        mysqli_close($conectar);
        echo json_encode($menusPadre);
    } else {
        echo "{\"data\":[{\"N_ESTAASIGNADO\":true,\"N_MNUID\":\"-1\",\"S_MNUNOMBRE\":\"-1\"}]}"; //para cuando no existen menus
    }

}

/**
 * Consigue todos los sub menus de un menu padre, para ser mostrados en una datatable
 * @param $menuIdPadre: ID del menu padre
 */
function getSubMenusDataTableGrupoAccesoMenu($menuIdPadre) {
    include_once '../php/GPAConsultas.php';
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $menuPadreId = mysqli_real_escape_string($conectar, $menuIdPadre);
    $sql = "SELECT N_MNUID, USR_N_MNUID, S_MNUNOMBRE FROM usr_menu WHERE USR_N_MNUID = '$menuPadreId' ";
    $subMenusPadreQuery = mysqli_query($conectar, $sql);
    if ($subMenusPadreQuery) {
        $i = 0;
        $grupoAccesoId = mysqli_real_escape_string($conectar, $_SESSION['grupoAccesoId']);
        $appId = mysqli_real_escape_string($conectar, $_SESSION['appIdAux']);
        while ($data = mysqli_fetch_assoc($subMenusPadreQuery)) {
            $subMenusPadre["data"][$i] = [
                'N_ESTAASIGNADO' => estaGrupoAccesoAsociadoAMenu($grupoAccesoId, $appId, $data["N_MNUID"]),
                'N_MNUID' => $data["N_MNUID"],
                'USR_N_MNUID' => $data["USR_N_MNUID"],
                'S_MNUNOMBRE' => utf8_encode($data["S_MNUNOMBRE"])
            ];
            $i++;
        }
        if (isset($subMenusPadre)) {
            mysqli_free_result($subMenusPadreQuery);
            mysqli_close($conectar);
            echo json_encode($subMenusPadre);
        } else {
            echo "{\"data\":[{\"N_ESTAASIGNADO\":false,\"N_MNUID\":\"-1\",\"S_MNUNOMBRE\":\"No existen sub menus asociados\"}]}";
        }
    } else {
        echo "Error consiguiendo getSubMenusDeMenuPadre";
    }
}

/**
 * Obtiene los menu padre de la aplicacion actual
 */
function getMenuPadreByApp() {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $appId = mysqli_real_escape_string($conectar, $_SESSION['appId']);
    $sql = "SELECT N_MNUID, N_APPID, S_MNUNOMBRE, S_MNUURL FROM usr_menu WHERE N_APPID = '$appId' AND USR_N_MNUID IS NULL ";
    $menusPadreQuery = mysqli_query($conectar, $sql);
    if ($menusPadreQuery) {
        while ($data = mysqli_fetch_assoc($menusPadreQuery)) {
            $menusPadre["data"][] = $data;
        }
    }

    if (isset($menusPadre)) {
        mysqli_free_result($menusPadreQuery);
        mysqli_close($conectar);
        echo json_encode($menusPadre);
    } else {
        echo "{\"data\":[{\"N_MNUID\":\"-1\",\"N_APPID\":\"-1\",\"S_MNUNOMBRE\":\"-1\",\"S_MNUURL\":\"-1\"}]}"; //para cuando no tiene menus disponibles
    }
}

/**
 * Obtiene los menu padre para una aplicacion en especifico
 * @param $appId: ID de la aplicacion
 */
function getMenuPadreByAppOtorgada($appId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idApp = mysqli_real_escape_string($conectar, $appId);
    $sql = "SELECT N_MNUID, N_APPID, S_MNUNOMBRE, S_MNUURL FROM usr_menu WHERE N_APPID = '$idApp' AND USR_N_MNUID IS NULL ";
    $menusPadreQuery = mysqli_query($conectar, $sql);
    if ($menusPadreQuery) {
        while ($data = mysqli_fetch_assoc($menusPadreQuery)) {
            $menusPadre["data"][] = $data;
        }
    }

    if (isset($menusPadre)) {
        mysqli_free_result($menusPadreQuery);
        mysqli_close($conectar);
        echo json_encode($menusPadre);
    } else {
        echo "{\"data\":[{\"N_MNUID\":\"-1\",\"N_APPID\":\"-1\",\"S_MNUNOMBRE\":\"-1\",\"S_MNUURL\":\"-1\"}]}"; //para cuando no tiene menus disponibles
    }
    include_once 'USRLogConsultas.php';
    añadirLogAplicacion($appId, $_SESSION['usrRut']); //registra la aplicacion en el log
}

/**
 * Elimina un menu y sus sub menus, y todo lo relacionado a el, mediante un procedimiento almacenado
 * @param $menuId
 */
function eliminarMenu($menuId) {
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'], $GLOBALS['pass'], $GLOBALS['db']);
    $idMenu = mysqli_real_escape_string($conectar, $menuId);
    $sql = "CALL eliminar_menu('$idMenu')";
    $procedureQuery = mysqli_query($conectar, $sql);
    if ($procedureQuery) {
        echo "exito";
    }
}