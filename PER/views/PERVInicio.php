<?php
if(!isset($_SESSION)) {
    session_start();
}
if (!isset($_SESSION['usrRut']) || !isset($_SESSION['empRut'])) {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
}
include '../../USR/php/EMPConsultas.php';
include '../../USR/php/USRConsultas.php';

$datosUsuario = json_decode(getDatosUsuario(), true);
$rut = $datosUsuario['N_USRRUT']. '-' .$datosUsuario['S_USRDIGITOVERIFICADOR'];
$urlLogEmpresa = json_decode(getUrlLogoEmpresa(), true)['S_EMPURLLOGO'];
$nombreUsuario = $datosUsuario['S_USRPRIMERNOMBRE']. ' ' .$datosUsuario['S_USRAPELLIDOPATERNO'];


?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="../js/funciones.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<head>
    <meta charset="UTF-8">
    <title>PERVINICIO</title>
</head>

<body>
<ul class="list-group">
    <li class="list-group-item"><?php echo $nombreUsuario ?></li>
    <li class="list-group-item"><?php echo $rut ?></li>
    <li class="list-group-item"><img src="<?php echo $urlLogEmpresa ?>" alt="icono empresa" "></li>
</ul>

<ul class="list-group" id="listaAplicaciones">
</ul>
<script>
    generarListaAplicaciones();

</script>
</body>

</html>
