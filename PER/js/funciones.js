function generarListaAplicaciones() {
    $.ajax({
        type: "GET",
        url: "../../USR/php/EMPConsultas.php",
        data: {actionEMP: 'getInfoAplicaciones'},
        success: function (response) {
            var jsonResponse = JSON.parse(response);
            var contenido;
            for (var key in jsonResponse) {
                if (jsonResponse.hasOwnProperty(key)) {
                    contenido = jsonResponse[key];
                }
            }

            for (var i = 0; i < contenido.length; i++) {
                añadirListaAplicaciones(contenido[i].S_APPNOMBRE, contenido[i].S_APPURLSPAGINAINCIO, contenido[i].S_APPURLICONO);
            }
        }
    });
}

function añadirListaAplicaciones(nombreApp, urlApp, iconoApp) {
    var parent = document.getElementById('listaAplicaciones');//obtiene el elemento al cual se va a anclar
    var elementoLista = document.createElement('li');
    elementoLista.setAttribute('class', 'list-group-item');
    var elementoUrlApp = document.createElement('a');
    var text = document.createTextNode(nombreApp); //añade el nombre de la app
    elementoUrlApp.append(text);
    elementoUrlApp.setAttribute('href', urlApp); //añade la url de la app
    var elementoUrlIconoApp = document.createElement('img');
    elementoUrlIconoApp.setAttribute('src', iconoApp); //añade la url del icono de la app
    elementoUrlIconoApp.setAttribute('alt', 'icono app');
    elementoUrlApp.appendChild(elementoUrlIconoApp);
    elementoLista.appendChild(elementoUrlApp);


    parent.appendChild(elementoLista);

}