/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     21-03-2017 14:45:30                          */
/*==============================================================*/


drop table if exists CMN_EMPRESA;

drop table if exists USR_APLICACION;

drop table if exists USR_DETALLEGRUPOACCESO;

drop table if exists USR_GRUPOACCESO;

drop table if exists USR_GRUPOACCESOUSUARIOS;

drop table if exists USR_LOGAPLICACION;

drop table if exists USR_LOGBACKEND;

drop table if exists USR_LOGMENU;

drop table if exists USR_MENU;

drop table if exists USR_USUARIO;

/*==============================================================*/
/* Table: CMN_EMPRESA                                           */
/*==============================================================*/
create table CMN_EMPRESA
(
   N_EMPRUT             int not null,
   S_EMPDIGITO          varchar(1),
   S_EMPRAZONSOCIAL     varchar(100),
   S_EMPURLLOGO         varchar(150),
   primary key (N_EMPRUT)
);

/*==============================================================*/
/* Table: USR_APLICACION                                        */
/*==============================================================*/
create table USR_APLICACION
(
   N_APPID              int not null,
   N_EMPRUT             int,
   S_APPNOMBRE          varchar(100),
   S_APPURLSPAGINAINCIO varchar(200),
   D_APPFECHACREACION   timestamp,
   N_APPESTADO          bool,
   S_APPURLICONO        varchar(500),
   primary key (N_APPID)
);

/*==============================================================*/
/* Table: USR_DETALLEGRUPOACCESO                                */
/*==============================================================*/
create table USR_DETALLEGRUPOACCESO
(
   N_APPID              int not null,
   N_GPASECUENCIAL      int not null,
   N_MNUID              int not null,
   D_DGAFECHACREACION   timestamp,
   primary key (N_APPID, N_GPASECUENCIAL, N_MNUID)
);

/*==============================================================*/
/* Table: USR_GRUPOACCESO                                       */
/*==============================================================*/
create table USR_GRUPOACCESO
(
   N_APPID              int not null,
   N_GPASECUENCIAL      int not null,
   S_GPANOMBRE          varchar(50),
   D_GPAFECHACREACION   timestamp,
   primary key (N_APPID, N_GPASECUENCIAL)
);

/*==============================================================*/
/* Table: USR_GRUPOACCESOUSUARIOS                               */
/*==============================================================*/
create table USR_GRUPOACCESOUSUARIOS
(
   N_USRRUT             int not null,
   N_GAUSECUENCIAL      int not null,
   N_APPID              int,
   N_GPASECUENCIAL      int,
   N_GAUESTADO          bool,
   D_GAUFECHACREACION   timestamp,
   primary key (N_USRRUT, N_GAUSECUENCIAL)
);

/*==============================================================*/
/* Table: USR_LOGAPLICACION                                     */
/*==============================================================*/
create table USR_LOGAPLICACION
(
   N_APPID              int not null,
   N_USRRUT             int not null,
   N_LAPSECUENCIAL      int not null,
   D_LAPFECHA           timestamp,
   primary key (N_APPID, N_USRRUT, N_LAPSECUENCIAL)
);

/*==============================================================*/
/* Table: USR_LOGBACKEND                                        */
/*==============================================================*/
create table USR_LOGBACKEND
(
   N_USRRUT             int not null,
   N_LBESECUENCIAL      int not null,
   D_LBEFECHA           timestamp,
   primary key (N_USRRUT, N_LBESECUENCIAL)
);

/*==============================================================*/
/* Table: USR_LOGMENU                                           */
/*==============================================================*/
create table USR_LOGMENU
(
   N_USRRUT             int not null,
   N_MNUID              int not null,
   N_LMESECUENCIAL      int not null,
   D_LMEFECHA           timestamp,
   primary key (N_USRRUT, N_MNUID, N_LMESECUENCIAL)
);

/*==============================================================*/
/* Table: USR_MENU                                              */
/*==============================================================*/
create table USR_MENU
(
   N_MNUID              int not null,
   USR_N_MNUID          int,
   N_APPID              int,
   S_MNUNOMBRE          varchar(50),
   S_MNUURL             varchar(50),
   D_MNUFECHACREACION   timestamp,
   primary key (N_MNUID)
);

/*==============================================================*/
/* Table: USR_USUARIO                                           */
/*==============================================================*/
create table USR_USUARIO
(
   N_USRRUT             int not null,
   N_EMPRUT             int,
   S_USRDIGITOVERIFICADOR varchar(1),
   S_USRCONTRASENA      varchar(1000),
   S_USREMAIL           varchar(100),
   S_USRPRIMERNOMBRE    varchar(50),
   S_USRSEGUNDONOMBRE   varchar(50),
   S_USRAPELLIDOPATERNO varchar(50),
   S_USRAPELLIDOMATERNO varchar(50),
   D_USRFECHACREACION   datetime,
   primary key (N_USRRUT)
);

alter table USR_APLICACION add constraint FK_RELATIONSHIP_16 foreign key (N_EMPRUT)
      references CMN_EMPRESA (N_EMPRUT) on delete restrict on update restrict;

alter table USR_DETALLEGRUPOACCESO add constraint FK_RELATIONSHIP_3 foreign key (N_APPID, N_GPASECUENCIAL)
      references USR_GRUPOACCESO (N_APPID, N_GPASECUENCIAL) on delete restrict on update restrict;

alter table USR_DETALLEGRUPOACCESO add constraint FK_RELATIONSHIP_4 foreign key (N_MNUID)
      references USR_MENU (N_MNUID) on delete restrict on update restrict;

alter table USR_GRUPOACCESO add constraint FK_RELATIONSHIP_12 foreign key (N_APPID)
      references USR_APLICACION (N_APPID) on delete restrict on update restrict;

alter table USR_GRUPOACCESOUSUARIOS add constraint FK_RELATIONSHIP_1 foreign key (N_USRRUT)
      references USR_USUARIO (N_USRRUT) on delete restrict on update restrict;

alter table USR_GRUPOACCESOUSUARIOS add constraint FK_RELATIONSHIP_13 foreign key (N_APPID, N_GPASECUENCIAL)
      references USR_GRUPOACCESO (N_APPID, N_GPASECUENCIAL) on delete restrict on update restrict;

alter table USR_LOGAPLICACION add constraint FK_RELATIONSHIP_6 foreign key (N_APPID)
      references USR_APLICACION (N_APPID) on delete restrict on update restrict;

alter table USR_LOGAPLICACION add constraint FK_RELATIONSHIP_7 foreign key (N_USRRUT)
      references USR_USUARIO (N_USRRUT) on delete restrict on update restrict;

alter table USR_LOGBACKEND add constraint FK_RELATIONSHIP_8 foreign key (N_USRRUT)
      references USR_USUARIO (N_USRRUT) on delete restrict on update restrict;

alter table USR_LOGMENU add constraint FK_RELATIONSHIP_10 foreign key (N_MNUID)
      references USR_MENU (N_MNUID) on delete restrict on update restrict;

alter table USR_LOGMENU add constraint FK_RELATIONSHIP_9 foreign key (N_USRRUT)
      references USR_USUARIO (N_USRRUT) on delete restrict on update restrict;

alter table USR_MENU add constraint FK_FATHER foreign key (USR_N_MNUID)
      references USR_MENU (N_MNUID) on delete restrict on update restrict;

alter table USR_MENU add constraint FK_RELATIONSHIP_5 foreign key (N_APPID)
      references USR_APLICACION (N_APPID) on delete restrict on update restrict;

alter table USR_USUARIO add constraint FK_RELATIONSHIP_15 foreign key (N_EMPRUT)
      references CMN_EMPRESA (N_EMPRUT) on delete restrict on update restrict;

