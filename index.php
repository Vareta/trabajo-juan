<?php
include 'code/utils.php';
?>

<!DOCTYPE html>
<html>
<script type="text/javascript" src="js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="USR/js/USRForm&Modal.js"></script>
<script type="text/javascript" src="js/boostrapValidator.js"></script>
<script type="text/javascript" src="js/bootbox.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min"></script>

<link rel="stylesheet" href="css/bootstrap.min.css">

<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesion</title>
</head>
<body>
<div class="container">
    <form method="post" data-toggle="validator" role="form" id="loginForm">
        <div class="form-group">
            <div class="form-group has-feedback">
                <input type="text" pattern="^([0-9]+-[0-9kK])$" class="form-control" id="rut" name="rut"
                       placeholder="Ej; 12345678-9"
                       data-error="El rut contiene caracteres invalidos. Ej de rut valido: 12345678-9" required>
                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                <div class="help-block with-errors"></div>
            </div>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password"
                   data-error="Este campo es obligatorio" required>
            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
            <div class="help-block with-errors"></div>
        </div>

    </form>
    <button type="button" id="btnIngresar" class="btn btn-primary" onclick="ingresar('#loginForm')"
            name="ingresar">
        Ingresar
    </button>

    <div class="alert alert-danger" role="alert" id="errorLogin" hidden></div>
</div>


</body>

<script type="text/javascript">
    $('#loginForm').validator(); //setea las validaciones
</script>
</html>
