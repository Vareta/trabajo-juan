<?php
session_start();
include '../../db/consultasUsuario.php';
include '../../db/db.php';
include '../../code/utils.php';
//echo $_SESSION['usrRut'];
//echo $_SESSION['empRut'];
?>
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/datatables.min.css"/>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script type="text/javascript" src="../../js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/datatables.min.js"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"></script>
<script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js"></script>
<script type="text/javascript" src="../../js/bootbox.min.js"></script>


<head>
    <meta charset="UTF-8">
    <title>Inicio admin</title>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">WebSiteName</a>
        </div>
        <ul class="nav navbar-nav" id="dynamicNavBar">
            <li><a href="#">Home</a></li>
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1
                    <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="../../index.php">Page 1-1</a></li>
                    <li><a href="#">Page 1-2</a></li>
                    <li><a href="#">Page 1-3</a></li>
                </ul>
            </li>
            <li><a href="#">Page 2</a></li>
            <li><a href="#">Page 3</a></li>
        </ul>
    </div>
</nav>

<script>
    function añadir(nombre) {
        var parent = document.getElementById('dynamicNavBar');
        var newElement = document.createElement('li');
        newElement.innerHTML = '<a href="#">' + nombre + '</a>';
        parent.appendChild(newElement);
    }

    function addToParent(parent, nombre) {
        var element = document.createElement('li');
        element.innerHTML = '<a href="#">' + nombre + '</a>';
        parent.appendChild(element);
    }

    function añadirHijos() {
        var parent = document.getElementById('dynamicNavBar');
        var dropdownElement = document.createElement('li');
        dropdownElement.setAttribute('class', 'dropdown');
        var dropdownToggleElement = document.createElement('a');
        dropdownToggleElement.setAttribute('class', 'dropdown-toggle');
        dropdownToggleElement.setAttribute('data-toggle', 'dropdown');
        dropdownToggleElement.setAttribute('href', '#');
        var text = document.createTextNode('Page 1');
        dropdownToggleElement.append(text);
        var dropdownSpanElement = document.createElement('span');
        dropdownSpanElement.setAttribute('class', 'caret');
        dropdownToggleElement.appendChild(dropdownSpanElement);
        var dropdownMenuElement = document.createElement('ul');
        dropdownMenuElement.setAttribute('class', 'dropdown-menu');
        addToParent(dropdownMenuElement, 'hola 1');
        addToParent(dropdownMenuElement, 'hola 2');
        addToParent(dropdownMenuElement, 'hola 3');
        dropdownElement.appendChild(dropdownToggleElement);
        dropdownElement.appendChild(dropdownMenuElement);
        parent.appendChild(dropdownElement);
    }

    $(document).ready(function () {
        añadir('Home');
        añadir('Casa');
        añadirHijos();
    });
</script>

<table id="example" class="display" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th>Rut</th>
        <th>Nombre</th>
        <th>Segundo Nombre</th>
        <th>Apellido Paterno</th>
        <th>Apellido Materno</th>
        <th>Ver</th>
    </tr>
    </tbody>
</table>
<script>
    $(document).ready(function () {
        $('#example').DataTable({
            "ajax": {
                "method": "GET",
                "url": "../../db/getAllUsuariosToJson.php"
            },
            "columns": [
                {"data": "N_USRRUT"},
                {"data": "S_USRPRIMERNOMBRE"},
                {"data": "S_USRSEGUNDONOMBRE"},
                {"data": "S_USRAPELLIDOPATERNO"},
                {"data": "S_USRAPELLIDOMATERNO"}
            ],
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
            },
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return data + ' - ' + row['S_USRDIGITOVERIFICADOR'];
                    },
                    "targets": 0
                }
            ]
        });
    });

</script>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#añadirUsrModal">
    Añadir
</button>

<!-- Modal -->
<div class="modal fade" id="añadirUsrModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Añadir usuario</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" id="añadirUsrForm" data-toggle="validator" role="form">
                    <div class="form-group has-feedback">
                        <label for="rut" class="col-sm-4 control-label">Rut</label>
                        <div class="col-sm-8">
                            <input type="text" pattern="^([0-9]+-[0-9kK])$" class="form-control" id="rut"
                                   name="rut" placeholder="Ej; 12345678-9"
                                   data-error="El rut contiene caracteres invalidos. Ej de rut valido: 12345678-9"
                                   required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="nombre" class="col-sm-4 control-label">Nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombre" name="nombre"
                                   placeholder="Alvaro"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="segundoNombre" class="col-sm-4 control-label">Segundo nombre</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="segundoNombre" name="segundoNombre"
                                   placeholder="Aldo"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoPaterno" class="col-sm-4 control-label">Apellido paterno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoPaterno" name="apellidoPaterno"
                                   placeholder="Ramirez"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="apellidoMaterno" class="col-sm-4 control-label">Apellido materno</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="apellidoMaterno" name="apellidoMaterno"
                                   placeholder="Riquelme"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mail" class="col-sm-4 control-label">Correo electronico</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control" id="mail" name="mail"
                                   placeholder="Riquelme"
                                   data-error="Correo invalido" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-4 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirmPassword" class="col-sm-4 control-label">Confirmar Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="confirmPassword"
                                   name="confirmPassword" data-match="#password"
                                   data-match-error="La contraseña no coincide"
                                   data-error="Este campo es obligatorio" required>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btnAñadir" class="btn btn-primary" onclick="postData()" name="register">
                    Añadir
                </button>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#añadirUsrModal').on('shown.bs.modal', function () { //para cuando inicial el modal.
        $('#rut').focus();
    })
    $('#añadirUsrForm').validator();//setea las validaciones

    $('#añadirUsrModal').on('hidden.bs.modal', function () { //cuando desaparece el modal, se resetea la informacion dentro
        $(this).find('form')[0].reset();
    })

    function postData() { //funcion para el botton Añadir en la modal de añadir usuario
        if (!$('#añadirUsrForm').validator('validate').has('.has-error').length) { //si todos los datos del form son validos
            event.preventDefault();
            // replace '#' by server-side script who get the post content
            $.post('addUsuario.php', $('#añadirUsrForm').serialize(), function (data, status, xhr) {
                console.info(data);
                console.info(status);
                console.info(xhr);
                // do something here with response;
                if (data.localeCompare("existeUsuario") == 0) {
                    bootbox.alert("Ya existe un usuario bajo ese rut.")
                } else if (data.localeCompare("existeMail") == 0) {
                    bootbox.alert("Ya existe un usuario con ese correo electronico.")
                } else if (data.localeCompare("rutUsrInvalido") == 0) {
                    bootbox.alert("El rut del usuario es invalido.")
                } else {
                    bootbox.alert("Usuario añadido correctamente.", function () {
                        window.location.replace("inicio_adm.php");
                    });
                    $('#añadirUsrModal').modal('toggle');
                }
            })
        }
    }
</script>

</body>
</html>
