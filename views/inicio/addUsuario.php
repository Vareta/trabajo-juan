<?php
session_start();
include '../../code/utils.php';
include '../../db/consultasUsuario.php';
include '../../db/consultasEmpresa.php';

/**
 * Respuestas a entregar en caso de:
 *
 * 'existeUsuario' --> Para cuando ya existe un usuario bajo el mismo rut.
 * 'existeMail' --> Para cuando ya existe el mail que se quiere ingresar.
 * 'rutUsrInvalido' --> Para cuando el rut del usuario es incorrecto.
 * 'rutEmpInvalido' --> Para cuando el rut de la empresa es incoorrecto. *
 *
 */

//se añade mysqli_real_escape_string para prevenir inyecciones sql
$conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
$rut =  mysqli_real_escape_string($conectar, $_POST['rut']);
$primerNombre = mysqli_real_escape_string($conectar, $_POST['nombre']);
$segundoNombre = mysqli_real_escape_string($conectar, $_POST['segundoNombre']);
$apellidoPaterno = mysqli_real_escape_string($conectar, $_POST['apellidoPaterno']);
$apellidoMaterno = mysqli_real_escape_string($conectar, $_POST['apellidoMaterno']);
$mail = mysqli_real_escape_string($conectar, $_POST['mail']);
$password = mysqli_real_escape_string($conectar, $_POST['password']);

echo $_SESSION['usrRut'];
$rutAux = preg_split("/-/", $rut); //separa el rut
if (!validaRutCompleto($rut)) { //rut invalido
    echo "rutUsrInvalido";
} else if (existeUsuario(intval($rutAux[0]))) { //Ya existe el usuario
    echo "existeUsuario";
} else {
    $hashPassword = password_hash($password, PASSWORD_DEFAULT);
    añadirUsuario(intval($rutAux[0]), $rutAux[1], $primerNombre, $segundoNombre, $apellidoPaterno, $apellidoMaterno,
        $mail, $hashPassword, intval($_SESSION['empRut']));

    echo "usuarioAñadido";
}