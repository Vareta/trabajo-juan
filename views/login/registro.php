<?php
include '../../code/utils.php';
session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registro</title>
</head>
<?php
$url = "http://". $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'];
if (strpos($url, 'error=username') !== false) {
    echo "Ya existe un usuario bajo el mismo rut";
}

$deboRegistrarUsuario = true;
if ($_SERVER['REQUEST_METHOD'] == 'POST') { //realiza las validaciones correspondientes
    if (isset($_POST['register'])) {
        if (!validaRut($_POST['rut'], $_POST['digVerificador'])) { //verifica si el rut es correcto
            $deboRegistrarUsuario = false;
            echo "Rut del usuario incorrecto. Vuelva a intentar <br>";
        } elseif ($_POST['password'] !== $_POST['passwordRepeticion']) { //verifica si las password son iguales
            $deboRegistrarUsuario = false;
            echo "Las contraseñas no coinciden. Vuelva a intentar <br>";
        } elseif (!validaRut($_POST['rutEmpresa'], $_POST['digVerificadorEmpresa'])) { //verifica si el rut de la empresa es correcto
            $deboRegistrarUsuario = false;
            echo "Rut de la empresa incorrecto. Vuelva a intentar <br>";
        }

        if ($deboRegistrarUsuario) {
            require  '../../code/login/register.php';
        }
    }
}
?>
<body>
<form action="registro.php" method="post" autocomplete="off">
    <b> USUARIO </b><br>
    <input type="text" name="rut" placeholder="Rut, Ej:123456789" required > <b> - </b>
    <input type="text" maxlength="1" name="digVerificador" required ><br>
    <input type="text" name="primerNombre" placeholder="Nombre" required ><br>
    <input type="text" name="segundoNombre" placeholder="Segundo Nombre" required ><br>
    <input type="text" name="apellidoPaterno" placeholder="Apellido Paterno" required ><br>
    <input type="text" name="apellidoMaterno" placeholder="Apellido Materno" required ><br>
    <input type="email" name="mail" placeholder="Correo Electronico" required ><br>
    <input type="password" name="password" placeholder="Contraseña" required ><br>
    <input type="password" name="passwordRepeticion" placeholder="Contraseña" required ><br>
    <b> EMPRESA </b><br>
    <input type="text" name="rutEmpresa" placeholder="Rut, Ej:80123456" required > <b> - </b>
    <input type="text" maxlength="1" name="digVerificadorEmpresa" required ><br>
    <input type="text" name="razonSocial" placeholder="Razon Social" required ><br>
    <button name="register">Registrar</button>
</form>
</body>
</html>