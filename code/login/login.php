<?php
if(!isset($_SESSION)) {
    session_start();
}
include '../../db/db.php';

if (isset($_POST['formData'])) {
    $datos = json_decode($_POST['formData'], true);
    $conectar = mysqli_connect($GLOBALS['host'], $GLOBALS['user'],$GLOBALS['pass'], $GLOBALS['db']);
    $rut =  mysqli_real_escape_string($conectar, $datos[0]['value']);
    $password = mysqli_real_escape_string($conectar, $datos[1]['value']);
    $rutAux = preg_split("/-/", $rut); //separa el rut (en este punto el rut siempre es valido y del tipo 12345678-9)

    include_once '../../USR/php/USRConsultas.php';
    include_once '../../code/utils.php';
    if (!validaRutCompleto($rut)) { //verifica si el rut es correcto
        echo "noValida"; //rut incorrecto
    } else if (!existeUsuario($rutAux[0])) {
        echo "noExiste"; //usuario no existe en la base de datos
    } else if (password_verify($password, getPasswordUsuario($rutAux[0])) == false) {
        echo "pwdIncorrecta";
    } else {
        $_SESSION['usrRut'] = $rutAux[0];
        $_SESSION['empRut'] = getRutEmpresaUsuario($rutAux[0]);
        include_once '../../USR/php/USRLogConsultas.php';
        añadirUsuarioLog($rutAux[0]); //registra al usuario en el log
        echo "exito";
    }

} else {
    header('Location: ../../USR/views/USRVError.php?noPermiso');
}