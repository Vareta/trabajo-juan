<?php
/**
 * Valida si un rut es correcto o no. Fuente:"https://es.wikipedia.org/wiki/Anexo:Implementaciones_para_algoritmo_de_rut#PHP"
 * Sólo contiene unas pequeñas modificaciones con respecto al original
 * @param $rut
 * @param $digVerificador
 * @return bool Retorna boolean con la respuesta
 */
function validaRut($rut, $digVerificador) {
    $s=1;
    for($m=0;$rut!=0;$rut/=10)
        $s=($s+$rut%10*(9-$m++%6))%11;
    $digVerificadorEsperado = chr($s?$s+47:75);
    //echo 'El digito verificador del rut ingresado es ',chr($s?$s+47:75);
    if (strcmp(strval(strtolower($digVerificador)), strtolower($digVerificadorEsperado)) == 0) {
        return true;
    } else {
        return false;
    }
}

/**
 * Valida si un rut es correcto o no. A diferencia de valida rut,
 * este recibe el rut en un solo parametro, Ej: 12345678-9
 * @param $rut String con el rut
 * @return bool Retorna boolean con la respuesta
 */
function validaRutCompleto($rut) {
    $rutAux = preg_split("/-/", $rut);

    if (count($rutAux) == 2) {
        return validaRut($rutAux[0], $rutAux[1]);
    } else {
        return false;
    }
}

function arrelgoMultUnico($arreglo, $columna) {
    foreach ($arreglo as $elemnto) {

    }
}